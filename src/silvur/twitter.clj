(ns silvur.twitter
  (:require [twitter.oauth :refer :all]
            [twitter.callbacks :refer :all]
            [twitter.callbacks.handlers :refer :all]
            [twitter.api.restful :as api]
            [twitter.request :refer (file-body-part status-body-part)]))

(defn make-creds [{:keys [consumer-key consumer-secret user-token user-token-secret] :as m}]
  (apply make-oauth-creds (vals m)))


(defn tweet [creds text & [file-path]]
  (if file-path
    (api/statuses-update-with-media :oauth-creds creds
                                    :body [(status-body-part text)
                                           (file-body-part file-path)])
    (api/statuses-update :oauth-creds creds :params {:status text})))
