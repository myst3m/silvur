(ns silvur.build
  (:gen-class)
  (:import [java.util.zip ZipInputStream])
  (:refer-clojure :exclude [compile])
  (:require [clojure.tools.build.api :as b]
            [clojure.tools.deps :as deps]
            [clojure.tools.build.api :as api]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.xml :as xml]
            [badigeon2.deploy :as bd]
            ;; [clojure.tools.deps.alpha :as alpha]
            [clojure.tools.cli :refer (parse-opts)]
            [taoensso.timbre :as log]
            [silvur.util :as util]))




;; ;; Refer to samples
;; ;; https://github.com/EwenG/badigeon/blob/master/sample/badigeon/sample.clj

;; ;; To omit unnecessaylog on building with exec-*
;; (log/set-level! :error)


;; (defn dir-deps []
;;   (let [dir (io/as-file (System/getenv "PWD"))]
;;     (loop [fs (file-seq dir)]
;;       (if (filter #(= "deps.edn" (str %)) fs)
;;         (str dir)
;;         (recur (file-seq (.getParent dir)))))))

;; (defn project-name [& [artifact-id]]
;;   (if artifact-id
;;     (last (str/split (str artifact-id) #"/"))
;;     (.getName (io/as-file (dir-deps)))))


;; ;;; for :exec-fn in deps.edn called with -X 

;; (defn exec-install [m]
;;   (let [{:strs [artifact version file-path pom-path a v f p]} (-> (update-vals m str) (update-keys name))
;;         artifact-path (or file-path f (str  (or a artifact) "-" (or v version) ".jar"))]
;;     (badigeon.install/install (symbol (or a artifact)) {:mvn/version (or v version "1.0.0")} artifact-path (or pom-path p "pom.xml") )
;;     (println (str "Installed artifact: " (.getName (io/file artifact-path))))))

;; (defn exec-jar [m]
;;   (let [{:strs [artifact version a v]} (-> (update-vals m str) (update-keys name))]
;;     (badigeon.jar/jar (symbol (or a artifact)) {:mvn/version (or v version)})
;;     (println (str "Created Jar: " (or a artifact) "-" (or v version) ".jar"))))


;; (defn exec-deploy [m]
;;   (let [{:strs [a artifact
;;                 v version
;;                 k gpg-key-id
;;                 f file-path
;;                 u user
;;                 p password]} (-> (update-vals m str) (update-keys name))
;;         a (or a artifact)
;;         v (or v version)
;;         k (or k gpg-key-id)
;;         f (or f file-path)
;;         u (or u user)
;;         p (or p password)]
;;     ;; As of 2023/4/1, sign is not supported due to the spec of gpg command arguments is changed
;;     (badigeon.deploy/deploy (symbol a) v
;;       [{:file-path f :extension "jar"}
;;        {:file-path "pom.xml" :extension "pom"}]
;;       {:id "clojars" :url "https://repo.clojars.org"}
;;       (conj {:allow-unsigned? true} (when (and u p) {:credentials {:username u :password p}})))))


;; (defn exec-uberjar [m]
;;   (let [{:strs [a v m main artifact version aot]} (-> (update-vals m str) (update-keys name))
;;         artifact (symbol (or a artifact))
;;         main (or m main)
;;         prj (project-name artifact)
;;         out-path (badigeon.bundle/make-out-path artifact version)]
;;     (println "Project:" prj)
;;     (badigeon.uberjar/bundle out-path
;;                              { ;; A map with the same format than deps.edn. :deps-map is used to resolve the project resources.
;;                               :deps-map (deps/slurp-deps (io/file "deps.edn"))
;;                               ;; Alias keywords used while resolving the project resources and its dependencies. Default to no alias.
;;                               :aliases [:1.7 :bench :test]
;;                               ;; The dependencies to be excluded from the produced bundle.
;;                               ;; :excluded-libs #{'org.clojure/clojure}
;;                               ;; Set to true to allow local dependencies and snapshot versions of maven dependencies.
;;                               :allow-unstable-deps? true
;;                               ;; When set to true and resource conflicts are found, then a warning is printed to *err*
;;                               :warn-on-resource-conflicts? false})
;;     ;; Recursively walk the bundle files and delete all the Clojure source files
;;     (let [ns-xs (map (comp symbol str) (filter #(re-find (re-pattern prj) (str %)) (all-ns)))]
;;       (require (symbol main))
;;       (when aot
;;         (println "Compile: " ns-xs (str out-path))
;;         (badigeon.compile/compile ns-xs {:compile-path out-path})
;;         (let [pat (re-pattern (str prj ".*\\.cljc*~*$"))]
;;           (badigeon.uberjar/walk-directory
;;            out-path
;;            (fn [dir f]
;;              (let [exact-file-name (str/replace (str f) (re-pattern (str dir)) "")]
;;                (cond
;;                  (re-find pat exact-file-name) (java.nio.file.Files/delete f)
;;                  (.endsWith (str f) ".SF") (java.nio.file.Files/delete f)
;;                  (.endsWith (str f) ".DSA") (java.nio.file.Files/delete f)
;;                  (.endsWith (str f) ".RSA") (java.nio.file.Files/delete f))))))))
    

;;     (println "Make MANIFEST")
;;     ;; Output a MANIFEST.MF file defining 'badigeon.main as the main namespace
;;     (spit (str (badigeon.utils/make-path out-path "META-INF/MANIFEST.MF"))
;;           (badigeon.jar/make-manifest main))

;;     ;; Return the paths of all the resource conflicts (multiple resources with the same path) found on the classpath.
;;     (badigeon.uberjar/find-resource-conflicts {:deps-map (deps/slurp-deps (io/file "deps.edn"))
;;                                                ;; Alias keywords used while resolving the project resources and its dependencies. Default to no alias.
;;                                                :aliases [:1.7 :bench :test]})

;;     (println "Merge resources")
;;     ;; Merge the resource conflicts (multiple resources with the same path) found on the classpath and handled by the provided \"resource-mergers\"
;;     (badigeon.uberjar/merge-resource-conflicts out-path {:deps-map (deps/slurp-deps (io/file "deps.edn"))
;;                                                          ;; Alias keywords used while resolving the project resources and its dependencies. Default to no alias.
;;                                                          :aliases [:1.7 :bench :test]
;;                                                          :resource-mergers badigeon.uberjar/default-resource-mergers})
;;     (println "Package:" (str out-path))
;;     ;; Zip the bundle into an uberjar
;;     (badigeon.zip/zip out-path (str out-path "-standalone.jar"))))



;; ;;;; Commands

;; (defn usage []
;;   (println "Usage:"))

;; (def option-schemas
;;   {:top [["-h" "--help" "This help"]
;;          ["-v" "--version VERION" "Version"
;;           :default "0.0.1"]]
;;    :protoc [["-h" "--help" "This help"]
;;             ["-o" "--out-dir DIR" "Output directory"
;;              :default "src-java"]]
;;    :javac [["-h" "--help" "This help"]
;;            ["-s" "--source-dir DIR" "Source directory"
;;             :default "src-java"]
;;            ["-p" "--class-path DIRs" "Colon-separated paths"
;;             :default "src-java"]
;;            ["-v" "--version VERION" "Version"
;;             :default "0.0.1"]]
;;    :compile [["-h" "--help" "This help"]
;;              ["-s" "--source-dir DIR" "Source directory"
;;               :default "src"]
;;              ["-t" "--target-dir DIR" "Target directory"
;;               :default "target/classes"]
;;              ["-v" "--version VERION" "Version"
;;               :default "0.0.1"]]
;;    :jar [["-h" "--help" "This help"]
;;          ["-a" "--artifact-id ID" "ID to be registered"
;;           :parse-fn symbol]
;;          ["-v" "--version VERION" "Version"
;;           :default "0.0.1"]]
;;    :deploy [["-h" "--help" "This help"]
;;             ["-f" "--file-path PATH" "File to be deployed"]
;;             ["-k" "--gpg-key-id" "GPG id to be signed"]]
;;    :uberjar [["-h" "--help" "This help"]
;;              ["-a" "--artifact-id ID" "ID to be registered"
;;               :parse-fn symbol]
;;              ["-m" "--main CLASS" "Main class"
;;               :parse-fn symbol]
;;              [nil "--aot" "Perform AOT"]
;;              ["-v" "--version VERION" "Version"
;;               :default "0.0.1"]]})



;; (defn javac
;;   ([]
;;    (javac {}))
;;   ([{:keys [arguments options summary] :or {options {:source-dir "src/main/java"}}}]
;;    (if (:help options)
;;      (println summary)
;;      (if-let [src (:source-dir options)]
;;        (do (println "Compiling" src)
;;            (badigeon.javac/javac (:source-dir options)
;;                                  (conj {:compile-path "target/classes"}
;;                                        (select-keys options [:javac-options]))))
;;        (println "Please specify source directory")))))


;; (defn compile
;;   ([]
;;    (compile {}))
;;   ([{:keys [arguments options summary]}]
;;    (let [{:keys [compile-path source-dir ns] :or {compile-path "target/classes"
;;                                                   source-dir "src"
;;                                                   ns (project-name)}} options
;;          pat (re-pattern ns)
;;          _ (->> (file-seq (io/as-file source-dir))
;;                 (filter #(or (.endsWith (str %) ".clj")
;;                              (.endsWith (str %) ".cljc")))
;;                 (map #(load-file (str %)))
;;                 (dorun))
;;          ns-xs (map (comp symbol str) (filter #(re-find pat (str %)) (all-ns)))]
;;      (badigeon.compile/compile ns-xs {:compile-path compile-path}))))

;; (defn protoc [op {:keys [arguments options summary]}]
;;   (if (:help options)
;;     (println summary)
;;     (let [dest (:out-dir options)
;;           proto-file (first arguments)]
;;       (io/make-parents (io/file dest "."))
;;       (badigeon.exec/exec "protoc" {:proc-args [proto-file "--java_out" dest "--grpc_java_out" dest]}))))




;; (defn jar
;;   ([]
;;    (jar {}))
;;   ([{:keys [arguments options summary]}]
;;    (if (:help options)
;;      (println summary)
;;      (let [{:keys [artifact-id]} options
;;            opts-map (clojure.set/rename-keys options {:version :mvn/version})]
;;        (do (println "Packaging jar:" opts-map)
;;            (when (.exists (io/as-file "pom.xml"))
;;              (io/delete-file "pom.xml"))
;;            (let [path (badigeon.jar/jar (or (:artifact-id options)  (symbol (project-name artifact-id)))
;;                                         opts-map)]
;;              (println "Completed")
;;              path))))))


;; (defn read-pom [jar-path]
;;   (with-open [zi (ZipInputStream. (io/input-stream jar-path))]
;;     (->> (repeatedly (constantly zi))
;;          (filter #(re-find #"pom.xml" (.getName (.getNextEntry %))))
;;          (first)
;;          (slurp)
;;          (.getBytes)
;;          (io/input-stream)
;;          (xml/parse)
;;          :content
;;          (filter (comp #{:groupId :artifactId :version} :tag))
;;          (map (juxt :tag (comp first :content)))
;;          (into {}))))



;; (defn deploy [{:keys [arguments options summary]}]
;;   (if (:help options)
;;     (println summary)
;;     (let [{:keys [file-path gpg-key-id]} options
;;           artifacts [{:file-path file-path :extension "jar"}
;;                      {:file-path "pom.xml" :extension "pom"}]]
;;       (if (and file-path gpg-key-id)
;;         (let [s-artifacts (badigeon.sign/sign artifacts gpg-key-id)
;;               {g :groupId a :artifactId v :version} (read-pom file-path)
;;               artifact-id (symbol (str g "/" a))]
;;           (println "Deploy:" artifact-id "version:" v)
;;           (if (and g a v)
;;             (try
;;               (badigeon.deploy/deploy artifact-id
;;                                       v
;;                                       s-artifacts
;;                                       {:id "clojars" :url "https://repo.clojars.org/"})
;;               (println "Completed")
;;               (catch Exception e (log/error e)))            
;;             (do (println "Jar file does not have enough info." )
;;                 (println "groupId:" g)
;;                 (println "artifactId:" a)
;;                 (println "version:" v))))
;;         (do (println "Please specify gpg-key-id and file-path" )
;;             (println)
;;             (println "ex. clojure -m silvur.build deploy -g theorems -a silvur -k myst3m@gmail.com -f target/silvur-1.9.10.jar")
;;             (println))))))

;; (defn uberjar [{:keys [arguments options summary] :as opts}]
;;   (if (:help options)
;;     (println summary)
;;     (if-not (:artifact-id options)
;;       (do (println "Need to specify artifact-id")
;;           (println summary))
;;       (let [{:keys [artifact-id version aot main]} options
;;             prj (project-name artifact-id)
;;             out-path (badigeon.bundle/make-out-path artifact-id version)]
;;         (println "Project" prj)
;;         (badigeon.uberjar/bundle out-path
;;                                  { ;; A map with the same format than deps.edn. :deps-map is used to resolve the project resources.
;;                                   :deps-map (deps/slurp-deps (io/file "deps.edn"))
;;                                   ;; Alias keywords used while resolving the project resources and its dependencies. Default to no alias.
;;                                   :aliases [:1.7 :bench :test]
;;                                   ;; The dependencies to be excluded from the produced bundle.
;;                                   ;; :excluded-libs #{'org.clojure/clojure}
;;                                   ;; Set to true to allow local dependencies and snapshot versions of maven dependencies.
;;                                   :allow-unstable-deps? true
;;                                   ;; When set to true and resource conflicts are found, then a warning is printed to *err*
;;                                   :warn-on-resource-conflicts? false})
;;         ;; Recursively walk the bundle files and delete all the Clojure source files
;;         (when aot
;;           (println "Compile")
;;           (clojure.pprint/pprint (select-keys
;;                                   (update opts :options assoc :compile-path (str out-path))
;;                                   [:arguments :options]))
;;           (compile (update opts :options assoc :compile-path (str out-path) :ns (project-name artifact-id)))
;;           (let [pat (re-pattern (str prj ".*\\.cljc*~*$"))]
;;             (badigeon.uberjar/walk-directory
;;              out-path
;;              (fn [dir f]
;;                (let [exact-file-name (str/replace (str f) (re-pattern (str dir)) "")]
;;                  (cond
;;                    (re-find pat exact-file-name) (java.nio.file.Files/delete f)
;;                    (.endsWith (str f) ".SF") (java.nio.file.Files/delete f)
;;                    (.endsWith (str f) ".DSA") (java.nio.file.Files/delete f)
;;                    (.endsWith (str f) ".RSA") (java.nio.file.Files/delete f)))))))
        

;;         (println "Make MANIFEST")
;;         ;; Output a MANIFEST.MF file defining 'badigeon.main as the main namespace
;;         (spit (str (badigeon.utils/make-path out-path "META-INF/MANIFEST.MF"))
;;               (badigeon.jar/make-manifest main))

;;         ;; Return the paths of all the resource conflicts (multiple resources with the same path) found on the classpath.
;;         (badigeon.uberjar/find-resource-conflicts {:deps-map (deps/slurp-deps (io/file "deps.edn"))
;;                                                    ;; Alias keywords used while resolving the project resources and its dependencies. Default to no alias.
;;                                                    :aliases [:1.7 :bench :test]})

;;         (println "Merge resources")
;;         ;; Merge the resource conflicts (multiple resources with the same path) found on the classpath and handled by the provided \"resource-mergers\"
;;         (badigeon.uberjar/merge-resource-conflicts out-path {:deps-map (deps/slurp-deps (io/file "deps.edn"))
;;                                                              ;; Alias keywords used while resolving the project resources and its dependencies. Default to no alias.
;;                                                              :aliases [:1.7 :bench :test]
;;                                                              :resource-mergers badigeon.uberjar/default-resource-mergers})
;;         (println "Package")
;;         ;; Zip the bundle into an uberjar
;;         (badigeon.zip/zip out-path (str out-path "-standalone.jar"))))))

;; (defn gen-settings-xml [{:syms [user u password p] :as opt}]
;;   (println (util/edn->xml {:settings {:servers {:server {:id "clojars" :username (or u user "your name") :password (or p password "your clojars token")}}}})))


;; (defn print-usage [summary]
;;   (println)
;;   (println "Usage:" "<command> [options]")
;;   (println)  
;;   (println "- Commands:")
;;   (println " " (str/join "," (map name (keys (dissoc option-schemas :top)))))
;;   (println)  
;;   (println "- Options:")
;;   (println summary))


;; (defn -main [& args]
;;   (log/set-level! :info)
;;   (let [{:keys [arguments options summary]} (parse-opts args (option-schemas :top))
;;         op (keyword (first arguments))]
;;     (if (or (:help options) (nil? op))
;;       (print-usage summary)
;;       (let [f (ns-resolve (the-ns 'silvur.build) (symbol (name op)))]
;;         (f (update (parse-opts args (option-schemas op)) :arguments rest))))
;;     (shutdown-agents)))


;; Due to loading from file 'clojure/tools/deps' as root dep, It cannot be native image

(defn- pom-template [& {:keys []}]
  [[:licenses
    [:license
     [:name "Eclipse Public License"]
     [:url "http://www.eclipse.org/legal/epl-v10.html"]]]])

(defn clean [_]
  (b/delete {:path "target"}))

(defn pom [{:syms [a v artifact version major minor org o]}]
  (let [lib (symbol (or a artifact))
        q-lib (if (qualified-symbol? lib) lib (symbol (str (name lib) "/" (name lib))))
        version (or v version)
        basis (b/create-basis {:project "deps.edn"})
        class-dir "target/classes"
        jar-file (format "target/%s-%s.jar" (name lib) version)
        organization (or o org)]
    (println "Writing POM:" lib version)
    (b/write-pom {:class-dir class-dir
                  :lib q-lib
                  :version version
                  :basis basis
                  :src-dirs ["src"]
                  :pom-data (pom-template :org organization)})))


(defn jar [{:syms [a v artifact version major minor org o]}]
  (let [lib (symbol (or a artifact))
        q-lib (if (qualified-symbol? lib) lib (symbol (str (name lib) "/" (name lib))))
        version (or v version)
        basis (b/create-basis {:project "deps.edn"})
        class-dir "target/classes"
        jar-file (format "target/%s-%s.jar" (name lib) version)
        organization (or o org)]
    (println "Creating jar:" lib version)
    (b/write-pom {:class-dir class-dir
                  :lib q-lib
                  :version version
                  :basis basis
                  :src-dirs ["src"]
                  :pom-data (pom-template :org organization)})
    (b/copy-dir {:src-dirs ["src" "resources"]
                 :target-dir class-dir})
    (b/jar {:class-dir class-dir
            :jar-file jar-file})))

(defn uber [{:syms [a v artifact version main major minor suffix]}]
  (let [lib (symbol (or a artifact))
        version (or v version)
        basis (b/create-basis {:project "deps.edn"})
        class-dir "target/classes"
        uber-file (cond-> "target/%s-%s"
                    suffix (str "-" suffix)
                    :always (str ".jar")
                    :always (format  (name lib) version))]
    (println "Creating standalone jar:" lib version)
    (clean nil)
    (b/copy-dir {:src-dirs ["src" "resources"]
                 :target-dir class-dir})
    (b/compile-clj {:basis basis
                    :ns-compile [(symbol main)]
                    :class-dir class-dir})
    (b/uber {:class-dir class-dir
             :uber-file uber-file
             :basis basis
             :main main})))

(defn deploy [{:syms [a artifact v version jar-file repository]
               :or {repository {:id "clojars"
                                :url "https://repo.clojars.org/"}}}]
  (let [lib (symbol (or a artifact))
        version (or v version)]
    
    (println "Deploying jar:" lib version)

    (bd/deploy {:basis (api/create-basis)
                :class-dir "target/classes"
                :lib (symbol (if (namespace lib) lib (symbol (name lib) (name lib))))
                :version (or version "0.1.0")
                :jar-file (or jar-file (str "target/" (name lib) "-" version ".jar"))
                :repository repository})))

(defn install [{:syms [a artifact v version classifier jar-file class-dir]}]
  (let [lib (symbol (or a artifact))
        version (or v version)]

    (b/install {:basis (api/create-basis)
                :lib lib
                :version version
                :jar-file (or jar-file (str "target/" (name lib) "-" version ".jar"))
                :class-dir (or class-dir "target/classes")})))

(log/set-min-level! :info)


