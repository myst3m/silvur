(ns silvur.oauth2.azure
  (:require [silvur.http :as http]
            [silvur.util :refer [edn->json]]
            [silvur.log :as log]))


(def sample-config
  {:port 9180,
   :auth-endpoint "https://login.microsoftonline.com/17bccb98-ce0c-411d-a8ae-76f0d6920643/oauth2/v2.0/authorize",
   :auth-options {:client-id "ac18d9d9e-b9e0-4cb3-b805-192d860914b3",
                  :response-type "code",
                  :scope ["openid"],
                  :redirect-uri "http://localhost:9180/oauth2/callback"}
   ,
   :token-endpoint "https://login.microsoftonline.com/17bccb98-ce0c-411d-a8ae-76f0d6920643/oauth2/v2.0/token",
   :token-options {:grant-type "authorization_code",
                   :client-id "ac18d9d9e-b9e0-4cb3-b805-192d860914b3",
                   :client-secret "z3.8Q~GgBrX-l0WctVgq60rZt.tc1lOCjwCYNbN6",
                   :redirect-uri "http://localhost:9180/oauth2/callback"}})


