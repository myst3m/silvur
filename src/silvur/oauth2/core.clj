(ns silvur.oauth2.core
  (:gen-class)
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :as pp]
            [clojure.data.json :as json]
            [org.httpkit.server :refer [run-server
                                        with-channel
                                        on-close
                                        on-receive
                                        websocket?
                                        send!]]
            [org.httpkit.client :as http]
            [mount.core :as mount :refer [defstate ]]
            [reitit.core :as r]
            [reitit.http :as rh]
            [reitit.http.interceptors.parameters :as params]
            [reitit.http.interceptors.muuntaja :as mu]
            [reitit.interceptor.sieppari]
            [muuntaja.core :as m]
            [reitit.ring :as ring]
            [clojure.core.async :refer [go]]
            [clojure.java.shell :refer [with-sh-env sh]]
            [clojure.tools.cli :refer [parse-opts]]
            [hiccup.core :refer :all]
            ;; [hiccup.form :refer [submit-button label email-field password-field]]
            [hiccup.page :refer [html5 include-css include-js]]
            [silvur.datetime :refer :all]
            [silvur.util :refer (uuid nrepl-start edn->json json->edn nrepl-stop)]
            [silvur.nio :as nio]
            [taoensso.timbre :as log]
            ;; [moncee.core :refer :all]
            [camel-snake-kebab.core :as csk]
            [camel-snake-kebab.extras :as cske]
            [buddy.core.codecs :as codecs]
            [buddy.sign.jwt :as jwt]))

;; {"access_token":"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJvYXV0aDJjbGkiLCJleHAiOjE2MjAzNjAwNDksInN1YiI6IjVlYTc3MWQzMzU3MDFlMDAwMTc3NmFlMSJ9.4p5Ez2mxlm1vpO4vBir7E9nF07mFGDSqeucRokd9nssKgVOIB-pbBf-3N4JNzyw4jDrW5B5iC3iPiAebkxJqtw","expires_in":7200,"refresh_token":"XZCIRPEXWQAI5DVLOUJD_G","token_type":"Bearer"}

;; Set default
;; (def mule-context {:auth-uri "https://anypoint.mulesoft.com/accounts/api/v2/oauth2/authorize"
;;                    :token-endpoint "https://anypoint.mulesoft.com/accounts/api/v2/oauth2/token"
;;                    :client-id "myclient"
;;                    :clinet-secret "mysecret"
;;                    :redirect-uri "https://anypoint.mulesoft.com/accounts/login/mulesoft-06863/providers/f41de43d-7d25-484d-9219-31f7236b9775/redirect"
;;                    :port 9180
;;                    })

(def accounts (atom []))

(defn load-accounts-to-memory! [cfg]
  (when-let [acs (:accounts cfg)]
    (reset! accounts (vec (map-indexed #(assoc %2 :index %1) acs))))
  cfg)

(defstate oauth2*
  :start (mount/args)
  :stop :not-configured)

(defstate config*
  :start
  (let [{:keys [auth-uri token-uri client-id client-secret redirect-uri port
                context config-file nrepl nrepl-port 
                scope
                config
                credential-path] :as opts} (mount/args)]
    (->> (cond-> (or config {})
           config-file (conj (read-string (slurp config-file)))
           credential-path (assoc-in [:credential-path] credential-path)
           port (assoc-in [:port] port)
           auth-uri (assoc-in [:auth-endpoint] auth-uri)
           token-uri (assoc-in [:token-endpoint] token-uri)
           client-id (assoc-in [:auth-options :client-id] client-id)
           redirect-uri (assoc-in [:auth-options :redirect-uri] redirect-uri)
           client-id (assoc-in [:token-options :client-id] client-id)
           client-secret (assoc-in [:token-options :client-secret] client-secret)
           redirect-uri (assoc-in [:token-options :redirect-uri] redirect-uri)
           scope (assoc-in [:auth-options :scope] scope)
           nrepl (assoc :nrepl nrepl)
           nrepl-port (assoc :nrepl-port nrepl-port))
         (load-accounts-to-memory!)
         ;;(alter-var-root #'context* merge)
         ))
  :stop {})

;; In memory database when mongodb  is not specified

(def idp-clients (atom {}))

(defonce channel-hub (atom {}))



(defn find-item [& kvs]
  (->> @accounts
       (reduce (fn [r item]
                 (when-let [checked (reduce (fn [x [k v]]
                                              (if (= v (k item))
                                                item
                                                (reduced nil)))
                                            {}
                                            (partitionv 2 kvs))]
                   (reduced checked)))
               {})))


;; (defn find-token [k v]
;;   (let []
;;     (reduce (fn [_ item]
;;               (when (= v (k item))
;;                 (reduced (assoc item :index i))))
;;             []
;;             @accounts)))

(defn store-item! [{:keys [index user password] :as item} & kvs]
  (log/debug "Store:" (seq (map vec (partition-all 2 kvs))))
  (let [m (into {} (map vec (partition-all 2 kvs)))
        z (dissoc (merge item m) :password)]
    (swap! accounts update-in [index] merge z)))


(defn page-content
  ([m]
   (page-content "" m))
  ([message {:keys [state response-type redirect-uri]}]
   [:form {:novalidate "" :role "form" :method "POST" :action "/oauth2/auth"}
    [:div {:class "field"}
     [:label {:class "label"} "User ID"]
     [:div {:class "control"}
      [:input {:class "input" :name "user" :type "user" :placeholder "you@example.com, or your ID"}]]]
    [:div {:class "field"}
     [:label {:class "label"} "Password"]
     [:div {:class "control"}
      [:input {:class "input" :name "password" :type "password" :placeholder "Password"}]]]
    [:input {:type "hidden" :name "state" :value state}]
    [:input {:type "hidden" :name "response_type" :value response-type}]
    [:input {:type "hidden" :name "redirect_uri" :value redirect-uri}]
    [:div {:class "field is-grouped"}
     [:div {:class "control"}
      [:input {:class "button is-link" :type "submit"}]]]
    [:div {:styles {:color "red"}}message]]))

(defn login-page [title & content]
  (log/debug "Login Page")
  (html5 {:ng-app "OAuth2" :lang "en"}
         [:head
   ,       [:title title]
          [:meta {:name "viewport" :content "width=device-width,initial-scale=1"}]
          (include-css "https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css")
          [:body
           [:br]
           [:div.container.is-fluid
            content ]]]))

(defn auth-handle [{:keys [params] :as req}]
  (log/info "Auth:" params)  
  (let [{:strs [client_id response_type state scope redirect_uri nonce]} params
        client-id (-> config* :auth-options :client-id)]
    
    (if (= client_id client-id)
      (do
        (swap! idp-clients assoc state params)
        {:status 200 :body (login-page "Authorization" (page-content {:response-type response_type
                                                                      :state         state
                                                                      :scope         scope
                                                                      :redirect-uri  redirect_uri}))})
      {:status 401 :body "Not authorized"})))

(defn login-handle [{:keys [params] :as req}]
  (log/info "Login:" params)
  (let [{:strs [response_type state scope redirect_uri user password]} params
        code (str (uuid))
        {idx :index correct-passwd :password :as item} (find-item :user user :password password)]
    (log/debug user password correct-passwd)
    (if (and idx (= password correct-passwd))
      (do (swap! idp-clients assoc code params)
          (swap! accounts assoc-in [idx :code] code)
          (swap! accounts vec)
          {:status 302
           :headers {"Location" (str redirect_uri "?code=" code "&" "state=" state)}})
      {:status 200 :body (->> {:response-type response_type
                               :state         state
                               :scope         scope
                               :redirect-uri  redirect_uri}
                              (page-content "User and/or Password incorrect")
                              (login-page "Authorization"))})))

(defn token-handle [{:keys [params]}]
  (log/info "Token:" params)
  (let [{:strs [grant_type code redirect_uri refresh_token]} params
        {:keys [index] :as item} (or (find-item :code code)
                                     (and (= grant_type "refresh_token")
                                          (find-item :refresh-token refresh_token)))]

    (jwt/sign {:iss "https://theorems.io" :aud "myclient" :exp (+ 3600 (datetime*)) } "secret")
    (log/debug "Index" index)
    (if index
      (let [token (str (uuid))
            refresh-token (str (uuid))
            state (get-in @idp-clients [code "state"])
            nonce (get-in @idp-clients [state "nonce"])
            
            id-token-payload {:iss "https://theorems.io"
                              :aud "myclient"
                              :exp (+ 3600 (datetime*))
                              :iat (datetime*)
                              :nonce nonce}
            id-token (jwt/sign id-token-payload "secret")]
        
        (log/debug @idp-clients [code "nonce"])
        (log/debug "id-token: " id-token-payload )
        (swap! accounts assoc-in [index :refresh-token] refresh-token)
        (swap! accounts assoc-in [index :access-token] token)
        (swap! accounts vec)
        {:status 200
         :headers {"Content-Type" "application/json"}
         :body (m/encode "application/json" {:access_token  token
                                             :token_type    "Bearer"
                                             :expires_in    3600
                                             ;; :scope         "general"
                                             :refresh_token refresh-token
                                             :id_token id-token
                                             ;;:t (datetime)
                                             })})
      
      
      {:status 401 :body "Invalid code"})))




;; Behave oauth2 client


(defn gen-auth-uri []
  (format "%s?%s"
          (:auth-endpoint config*)
          (->> (:auth-options config*)
               (reduce (fn [r [k v]]
                         (str r "&" (csk/->snake_case_string k) "=" (if (string? v) v (str/join "%20" v)))) ""))
          ;; (str/join "&" (map #(str/join "=" (map name %)) (->> (dissoc (:auth-options context) :scope)
          ;;                                                (cske/transform-keys csk/->snake_case ))))
          ;; (str/join "%20" (:scope (:auth-options context)))
          )

  
  ;; (str (:auth-endpoint context)
  ;;   "?"
  ;;   (str/join "&" (map #(str/join "=" (map name %)) (->> (dissoc (:auth-options context) :scope)
  ;;                                                        (cske/transform-keys csk/->snake_case ))))
  ;;   "&"
  ;;   "scope="
  ;;   (str/join "%20" (:scope (:auth-options context))))
  )

;; %20read:api_policies%20manage:api_proxies%20read:servers%20read:exchange%20read:secrets%20read:secrets_metadata

; https://anypoint.mulesoft.com/accounts/api/v2/oauth2/authorize?client_id=ac158d2a638946c3818135ee18a451e5&scope=manage:api_configuration%20manage:api_policies&response_type=code&redirect_uri=http://localhost:9180/oauth2/callback&nonce=123456



(defn get-token [token-endpoint token-headers token-opts]
  (let [backup-cred oauth2*]
    (mount/stop #'oauth2*)
    (-> @(http/post token-endpoint
                    { :headers (merge {"Content-Type" "application/x-www-form-urlencoded"} token-headers)
                     ;;:basic-auth [(:client_id token-opts) (:client_secret token-opts)]
                     ;; :body (edn->json :snake token-opts)
                     ;;:body (edn->json token-opts)
                     :form-params (cske/transform-keys csk/->snake_case token-opts)})
        (as-> ret
            (if (= 200 (:status ret))
              (do (-> (mount/with-args #'oauth2* (json->edn (:body ret)))
                      (mount/start))
                  (log/info "Successfully got access token")
                  oauth2*)
              (do (log/warn "Failed to get token")
                  (log/warn (:status ret) (:body ret))
                  (mount/start (mount/with-args #'oauth2* backup-cred))))))))

(defn refresh-access-token []
  (log/info "Refreshing access token")
  (let [{:keys [credential-path]} config*]
    (let [cred (get-token (:token-endpoint config*)
                          (:token-headers config*)
                          (-> (select-keys oauth2* [:refresh-token])
                              (assoc :grant-type "refresh_token" :client-id (-> config* :auth-options :client-id))))] 
      (when credential-path
        (spit credential-path cred)
        (log/info "Saved access token in refresh process")))))


(defn callback-handle [{:keys [params status body] :as req}]
  (let [{:strs [code state]} params
        {:keys [token-endpoint token-options token-headers credential-path]} config*
        token-opts (assoc token-options :code code)]
    (log/debug "ENDPOINT" token-endpoint)
    (log/debug  "BODY:" token-opts)
    (if code
      (let [cred (get-token token-endpoint token-headers token-opts)]
        (when (and (seq cred) credential-path)
          (spit credential-path cred))
        {:status 200 :body (edn->json oauth2*) :headers {"Content-Type" "application/json"}})
      {:status 400 :body (edn->json oauth2*) :headers {"Content-Type" "application/json"}})))






;; Make sure to use ring-handler in reitit.http , otherwise , 406 returned

;; Your resource directory is on classpath
;; root-dir is the relative path of resource directory

;; Server:
;;  $ java -cp target/app.jar:. app.core -r tmp
;;  README.md should be in ./tmp 
;; Client:
;;  $ wget http://localhost:8090/README.md

(defn debug-body-intercepter []
  {:name ::debug-body
   :enter (fn [ctx]
            (let [req (:request ctx)
                  {:keys [headers]} req]
              (cond
                (and (:body req) (= (headers "content-type") "application/json"))
                (log/debug (with-out-str  (clojure.pprint/pprint (m/decode "application/json" (str/join (map char (.bytes (:body req))))))))
                :else
                (log/debug (with-out-str  (clojure.pprint/pprint req))))
              ctx))})

(defn no-handle [req]
  (clojure.pprint/pprint  (m/decode "application/json" (str/join (map char (.bytes (:body req))))))
  {:status 200})

(defn oidc-dynamic-registration-handler [req]
  {:status 201
   :body (edn->json {:client_id "myclient"
                     :client_secret "mysecret"
                     :client_secret_expires_at (+ 3600 (datetime*))})})

;;  https://anypoint.mulesoft.com/login/domain/mulesoft-06863

(defn oidc-userinfo [{:keys [params] :as req }]
  (log/debug req)
  ;; Actually request is HTTP Long polling. with-channel can be used.
  
  {:status 200
   :body (edn->json {:sub "tm00001"
                     :name "Tsutomu Miyashita"
                     :given_name "Tsutomu"
                     :family_name "Miyashita"
                     :email "myst3m@gmail.com"})}
  )


(defn oidc-register [req]
  (log/debug "register:" req)
  (log/info "instrospect:" (:params req))
  {:status 201
   :body (edn->json :snake {:client_id (str (datetime*))
                            :client_secret (str (datetime*))
                            :client_secret_expires_at 300
                            :registration_client_url "http://mule-dev.com/oauth2/connect/register"
                            :redirect_urls ["http://mule-dev.com/oauth2/callback1"
                                            "http://mule-dev.com/oauth2/callback2"]})})

(defn oidc-introspect [req]
  (log/debug "register:" req)
  (log/debug "instrospect:" (select-keys req [:body :headers]))
  (let [{:keys [headers]} req
        {:strs [authorization]} headers
        acct (find-item :access-token (str/replace authorization #"Bearer[ ]+" ""))]
    (log/debug "found account:" acct)
    (doto {:status (parse-long (get-in req [:params "token"] "200"))
           :body (edn->json {:token_type "Bearer",
                             :preferred_username (or (:user acct) "guest-preferred")
                             :email (or (:email acct) "guest@example.com"),
                             :sub (or (:user acct) "guest")
                             :exp (+ (datetime*) 10)})}
      (log/info))))



(def app
  (rh/ring-handler
   (rh/router
    ["" {:muuntaja m/instance}
     ["/timeout/:wait-time" {:get (fn [req]
                                    (log/debug (:headers req))
                                    (log/debug (str "Requested: " (parse-long (-> req :path-params :wait-time))))
                                    (Thread/sleep (parse-long (-> req :path-params :wait-time)))
                                    {:status 200
                                     :body (str "waited: " (-> req :path-params :wait-time) " ms")
                                     }

                                    )}]
     ["/default" {:get (fn [req] (clojure.pprint/pprint req) {:status 200 :body "ok"})
                  :post (fn [req] (clojure.pprint/pprint req))}]
     ["/oauth2/connect"
      ["/login" {:post oidc-dynamic-registration-handler}]
      ["/issue" {:get no-handle}]
      ["/userinfo" {:get oidc-userinfo}]
      ["/register" {:post oidc-register
                    :get oidc-register
                    :put (fn [req] (clojure.pprint/pprint req) {:status 200})}]
      ["/register/:client-id" {:put (fn [req]
                                      (clojure.pprint/pprint (:body req))
                                      {:status 200
                                       :body (edn->json :snake {:client-id "1696163842852"
                                                                :client-secret (str (datetime*))})})
                               :post (fn [req] (clojure.pprint/pprint req) {:status 200})
                               :delete (fn [req]
                                         (clojure.pprint/pprint req)
                                         {:status 200})}]
      ["/introspect" {:get oidc-introspect
                      :post oidc-introspect}]]
     ["/oauth2/auth" {:get auth-handle
                      :post login-handle}]
     ["/oauth2/token" {:post token-handle}]
     ["/oauth2/callback" {:get callback-handle}]])
   (ring/routes
    (ring/create-resource-handler {:root (:root-dir config*)
                                   :path "/"})
    (ring/create-default-handler))
   {:interceptors [(mu/format-interceptor)
                   (params/parameters-interceptor)
                   (debug-body-intercepter)]
    :executor     reitit.interceptor.sieppari/executor}))




(defstate server*
  :start  (run-server #'app (merge {:ip "0.0.0.0" :port 9180} (update-keys (select-keys (mount/args) [:ip :port]) {:ip :host}))) ;;(start)
  :stop (server*))


(defstate nrepl
  :start (when (:nrepl config*) (nrepl-start :ip "localhost" :port (or (:nrepl-port (mount/args)) 7888)))
  :stop (nrepl-stop))


(defn dev []
  (mount/start (mount/with-args {:config-file "idp.edn"}))
  )






(defn run-oauth2-authorization-code [{:keys [config credential credential-path] :as opts}]
  (println)
  (println "== Context: " )
  (mount/start (mount/with-args #'config* opts))
  (println (with-out-str (clojure.pprint/pprint config*)))
  (println "== Auth URI:\n" (gen-auth-uri))
  (mount/start (mount/with-args #'server* opts))
  (load-accounts-to-memory!)
  (cond 
    credential (do (log/info "Credential has been configured")
                   (mount/start (mount/with-args #'oauth2* credential)))
    credential-path (let [cred (if (silvur.nio/exists? (io/file credential-path))
                                 (do
                                   (log/info "Credential has been configured")
                                   (read-string (slurp credential-path)))
                                 (do
                                   (log/info "Need to perform authentication.")
                                   {}))]
                      (mount/start (mount/with-args #'oauth2* cred)))))










