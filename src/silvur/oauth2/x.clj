(ns silvur.oauth2.x
  (:require [silvur.http :as http]
            [silvur.util :refer [edn->json]]
            [silvur.log :as log]
            [mount.core :as mount]
            [silvur.oauth2.core :as oauth2 :refer [oauth2*]]))


(def sample-config
  {:port 9180,
   :auth-endpoint "https://twitter.com/i/oauth2/authorize",
   
   :auth-options {:client-id "dmoxTE92TS1TbU5rWWRQaS1ER1U6MTpjaQ",
                  :response-type "code",
                  :scope ["tweet.read" "users.read" "tweet.write" "offline.access"],
                  :code-challenge "challenge"
                  :code-challenge-method "plain"
                  :state "state"
                  :redirect-uri "http://localhost:9180/oauth2/callback"}
   ,
   :token-endpoint "https://api.x.com/2/oauth2/token",
   :token-headers {"Authorization" "Basic ZG1veFRFOTJUUzFUYlU1cldXUlFhUzFFUjFVNk1UcGphUTpJWnM3Q1VaeDRfOEdUZHJ6MkxYdjJoVWhlOE9Vb2xnUEJmTDdwNERMeGE3ak9YOFVGbw=="}
   :token-options {:grant-type "authorization_code",
                   :client-id "dmoxTE92TS1TbU5rWWRQaS1ER1U6MTpjaQ",
                   :code-verifier "challenge"
                   :client-secret "IZs7CUZx4_8GTdrz2LXv2hUhe8OUolgPBfL7p4DLxa7jOX8UFo",
                   :redirect-uri "http://localhost:9180/oauth2/callback"}})

(def last-tweets (atom '()))

(defn tweet
  ([text]
   (tweet text 1 true))
  ([text preserve-history-count]
   (tweet text preserve-history-count true))
  ([text preserve-history-count update-last-tweet?]
   (if-let [access-token (:access-token oauth2*)]
     (tweet text access-token preserve-history-count update-last-tweet?)
     (log/info "OAuth2 access token not available. Conduct authorization code flow.")))
  ([text access-token preserve-history-count update-last-tweet?]
   (letfn [(-tweet [txt]
             (when-not (some #(= % txt) @last-tweets)
               (let [ret (http/post "https://api.x.com/2/tweets" {:headers {"Content-Type" "application/json"
                                                                            "Authorization" (str "Bearer " access-token)}
                                                                  :body (edn->json {:text txt})})] 
                 (log/info "tweeted" txt)
                 (when update-last-tweet?
                   (swap! last-tweets (fn [a x] (cons x ((if (< 2 (count a)) butlast identity) a))) txt))
                 ret)))]
     (loop [i 5 ret (-tweet text)]
       (let [{:keys [status body]} ret]
         (cond
           (nil? status) (do (log/debug "Not tweeted since given text = last one"))
           (= 401 status) (if (= i 0)
                            (log/warn "Failed to tweet even token refreshed")
                            (do
                              (log/warn "Try to refresh token" i)
                              (oauth2/refresh-access-token)
                              (recur (dec i) (-tweet text))))
           (<= 200 status 299) (log/info "tweet status: " status)
           :else (log/warn "received errors" status body)))))))
