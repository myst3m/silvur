;; *   Silvur
;; *
;; *   Copyright (c) Tsutomu Miyashita. All rights reserved.
;; *
;; *   The use and distribution terms for this software are covered by the
;; *   Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
;; *   which can be found in the file epl-v10.html at the root of this distribution.
;; *   By using this software in any fashion, you are agreeing to be bound by
;; * 	 the terms of this license.
;; *   You must not remove this notice, or any other, from this software.

(ns silvur.datetime
  (:require [goog.string :as gstr]))



;; Minimum unit is 'second'
(def UTC "UTC")
(def JST "Asia/Tokyo")
(def NYC "America/New_York")

;; Offset is (UTC - Local)
(defn tz-offset [tz-symbol]
  (or ({UTC 0
        JST (- (* 9 60))
        NYC (* 4 60)} tz-symbol)
      0))

(defn local-tz-offset []
  (.getTimezoneOffset (js/Date.)))


(def FORMAT {:JP "YYYY/MM/dd HH:mm:ss"
             :US "MM/dd/YYYY HH:mm:ss"})


(defonce ^:dynamic *tz* JST)
(defonce ^:dynamic *precision* 1)



(declare adjust)

(defn set-default-precision! [x]
  (set! *precision* (condp = x
                      :second 1000
                      :milli 1
                      1)))

(defn set-default-tz! [tz]
  (set! *tz* tz))

(defrecord DateTime [instance tz])

(defmulti -datetime (fn [arg & _]  (type arg)))

;; Input TZ is local
(defmethod -datetime js/Number [arg & rest]
  (let [offset (tz-offset *tz*)]
    (if (and (>= 9999 arg 1970) (not (instance? cljs.core/Keyword (first rest))))
      (map->DateTime {:instance (js/Date. arg
                                          (dec (nth rest 0 1))
                                          (nth rest 1 1)
                                          (nth rest 2 0)
                                          (- (nth rest 3 0) (local-tz-offset))
                                          (nth rest 4 0)) :tz *tz*})
      
      (map->DateTime {:instance (js/Date. (- (condp = (first rest)
                                               :second (* *precision* arg)
                                               :milli arg
                                               arg)
                                             ;;(minute (local-tz-offset))
                                             (minute (tz-offset *tz*))))
                      :tz *tz*}))))

(defmethod -datetime js/String [arg & rest]
  (map->DateTime {:instance (js/Date. arg)}))

(defmethod -datetime cljs.core/Keyword [arg & rest]
  (let [{i :instance } (apply -datetime rest)
        dt ((juxt #(.getFullYear %)
                  #(inc (.getMonth %))
                  #(.getDate %)
                  #(.getHours %)
                  #(.getMinutes %)
                  #(.getSeconds %)
                  #(.getMilliseconds %)) i)
        formatter {:basic-iso-date "%4d-%02d-%02d+0000"
                   :iso-date "%4d-%02d-%02d+00:00"
                   :iso-date-time "%4d-%02d-%02dT%02d:%02d:%02d.%03d+00:00"}]

    (apply gstr/format (or (formatter arg )
                           (formatter :iso-date-time)) dt)))


;; (defmethod -datetime ZonedDateTime [arg & rest] arg)

;; (defmethod -datetime clojure.lang.LazySeq [arg & _]
;;   (map -datetime arg))

(defmethod -datetime cljs.core/PersistentVector [arg & _]
  (apply -datetime arg))

(defmethod -datetime js/Date [arg & rest]
  (-datetime (.getTime ^js/Date arg) :milli))

;; (defmethod -datetime clojure.lang.PersistentArrayMap [arg & _]
;;   (-datetime (:datetime arg)))

;; (defmethod -datetime clojure.lang.PersistentHashMap [arg & _]
;;   (-datetime (:datetime arg)))

;; (defmethod -datetime clojure.lang.IFn [f & rest]
;;   (adjust (apply -datetime (or rest [nil])) (f)))

(defmethod -datetime silvur.datetime/DateTime [arg & rest]
  arg)

(defmethod -datetime :default [arg & rest]
  (map->DateTime {:instance (js/Date.) :tz UTC}))


(defn datetime
  ([]
   (-datetime (.getTime (js/Date.))))
  ([arg & rest]
   (apply -datetime arg (flatten [rest]))))

(defn datetime*
  ([]
   (datetime* (.getTime (js/Date.))))
  ([arg & rest]
   (-> (apply -datetime arg rest)
       :instance
       (.getTime)
       (+ (minute (tz-offset *tz*)))
       (* *precision*)
       ;;(- (minute (local-tz-offset)))
       )))


(defn tz-> [zid dt]
  (map->DateTime {:instance (js/Date. (+ (datetime* dt)
                                         (- (minute (tz-offset (:tz dt))))
                                         (- (minute (tz-offset zid)))
                                         ;; (minute (local-tz-offset))
                                         ))
                  :tz zid}))

;; (defn date<- ^Date [^ZonedDateTime zdt]
;;   (Date/from (.toInstant zdt)))

(defn inst<- [z]
  (.-instance z))


(defn vec<- [z]
  ((juxt #(.getFullYear %)
         #(inc (.getMonth %))
         #(.getDate %)
         #(.getHours %)
         #(.getMinutes %)
         #(.getSeconds %)
         #(.getMilliseconds %)) (inst<- z)))

;; (defn calendar<- ^Instant [^ZonedDateTime zdt]
;;   (GregorianCalendar/from zdt))



(defmulti +time (fn [v _ & _] (type v)) )
(defmethod +time js/Number [z delta & deltas]
  (datetime* (apply +time (datetime z) delta deltas)))

(defmethod +time silvur.datetime/DateTime [z delta & deltas]
  (+  (datetime* z)
      (* *precision* (+ delta (reduce + deltas)))))



;; (defn duration [i]
;;   (Duration/ofSeconds (quot (* *precision* i) 1000)))

(defprotocol TimeExchange
  (minutes-of-week [zdt])
  (first-date-of-week [zdt])
  (adjust [zdt duration])
  (-year [i])
  (-month [i])
  (-day [i])
  (-hour [i])
  (-minute [i])
  (-second [i])
  (-day-of-week [i]))

(extend-protocol TimeExchange
  js/Number
  (-day [i]
    (quot (* 1000 60 60 24 i) *precision*))
  (-hour [i]
    (quot (* 1000 60 60 i) *precision* ))
  (-minute [i]
    (quot (*  1000 60 i) *precision*))
  (-second [i]
    (quot (* 1000 i) *precision*))
  (first-date-of-week [i]
    (first-date-of-week (datetime i)))
  (minutes-of-week [i]
    (minutes-of-week (datetime i)))
  (adjust [v duration]
    (let [offset (-minute (tz-offset *tz*))]
      (datetime* (-  (* duration  (quot (+ v offset) (quot duration *precision*))) offset))))

  js/Date
  (-day [i] 
    (.getDate i))
  (-hour [i]
    (.getHours i))
  (-minute [i]
    (.getMinutes i))
  (-second [i]
    (.getSeconds i))
  (-day-of-week [i]
    (.getDay i))
  
  silvur.datetime.DateTime
  (-year [z]
    (.getFullYear (inst<- z)))
  (-month [z]
    (inc (.getMonth (inst<- z))))
  (-day [z]
    (.getDate (inst<- z)))
  (-hour [z]
    (.getHours (inst<- z)))
  (-minute [z]
    (.getMinutes (inst<- z)))
  (-second [z]
    (.getSeconds (inst<- z)))
  (-day-of-week [z]
    (.getDay (inst<- z)))
  (minutes-of-week [z]
    (+
     (* (-minute z))
     (* 60 (-hour z))
     (* 60 24 (mod (-day-of-week z) 7))
     (tz-offset (:tz z))))
  (first-date-of-week [z]
    (-> (- (datetime* z) (-minute (minutes-of-week z)) )
        (adjust (-minute 1))))) ;; Sunday = 0

(defn year [i] (-year i))
(defn month [i] (-month i))
(defn day
  ([] (-day 1)) 
  ([i] (-day i)))
(defn hour
  ([] (-hour 1)) 
  ([i] (-hour i)))
(defn minute
  ([] (-minute 1)) 
  ([i] (-minute i)))


;; (defn this-month [& args]
;;   (apply datetime (concat ((juxt #(.getYear ^ZonedDateTime %)
;;                                  #(.getMonthValue ^ZonedDateTime %))
;;                            (datetime))
;;                           args)))

;; (defn this-year [& args]
;;   (apply datetime (concat ((juxt #(.getYear ^ZonedDateTime %)) (datetime)) args)))

(defn today [& args]
  (apply datetime (take 3 (vec<- (datetime)))))
(defn yesterday [& args]
  (apply datetime (update-in (vec (take 3 (vec<- (datetime)))) [2] dec)))

;; (defmacro this-month* [& args]
;;   `(datetime* (this-month ~@args)))
;; (defmacro this-year* [& args]
;;   `(datetime* (this-year ~@args)))
;; (defmacro today* [& args]
;;   `(datetime* (today ~@args)))
;; (defmacro yesterday* [& args]
;;   `(datetime* (yesterday ~@args)))

(defn time-seq [latest delta]
  (lazy-seq
   (cons (adjust latest delta)
         (time-seq (+time (adjust latest delta) (- delta)) delta))))


;; (defn time-period-seq
;;   ([]
;;    (time-period-seq (datetime) (minute 1)))
;;   ([latest]
;;    (time-period-seq latest (minute 1)))
;;   ([latest delta]
;;    (lazy-seq
;;     (let [end (adjust latest (minute 1))
;;           start (adjust latest delta)]
;;       (cons  [start end] (time-period-seq (+time start (- (minute 1))) delta))))))

;; (defn- -offset [zid  zdt]
;;   (-> (tz-> zid zdt)
;;       (.getOffset)
;;       (.getTotalSeconds)
;;       (quot (hour 1))))

;; (defmulti summer-time (fn [z h] (class z)))
;; (defmethod summer-time java.lang.String [z h]
;;   (summer-time (ZoneId/of z) h))


;; (defmethod summer-time :default [zid h]
;;   (binding [*precision* 1000]
;;     (+ h (-offset zid (datetime 2014 1 1)) (- (-offset zid (datetime))))))


