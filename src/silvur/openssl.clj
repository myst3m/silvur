(ns silvur.openssl
  (:require [clojure.java.shell :refer [sh with-sh-dir with-sh-env]]
            [clojure.tools.cli :refer (parse-opts)]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.java.process :as proc]
            [silvur.util :as util]
            [babashka.fs :as fs]))

(def ^:dynamic *work-dir*)
(def ^:dynamic *ca-root* "CA")

(defn work-dir [& paths]
  (str/join "/" (cons *work-dir* paths)))

(defn ca-dir [& paths]
  (str/join "/" (cons *ca-root* paths)))

(def ^:dynamic *debug* false)
(def ^:dynamic *dry* false)

(defn debug! [& [on-or-off]]
  (alter-var-root #'*debug* (constantly (or (nil? on-or-off) (#{true :on} on-or-off)))))

(defn set-ca-root! [path]
  (alter-var-root #'*ca-root* (constantly path)))

(defn debug [cmd]
  (when *debug*
    (println "==>" (if (sequential? cmd)
                        (str/join " "cmd)
                        cmd))))

(defn gen-file [type cmd-array file-name]
  (let [target-path (io/file *work-dir* file-name)]
    (io/make-parents target-path)
    (debug cmd-array)
    (if *dry*
      (str/join " " cmd-array)
      (let [
            {:keys [exit out err]} (apply sh cmd-array)]
        (if (= 0 exit)
          (do (println "created" (name type) (str target-path))
              (spit target-path out))
          (println err))))))


(defn gen-private-key [& {:keys [length passout name]
                          :or {length 2048
                               name "private-key.pem"}}]
  
  (let [cmd-array (cond-> ["openssl" "genrsa" ]
                    passout (concat ["-aes256" "-passout" (str "pass:" passout)])
                    length (concat [(str length)]))]
    (gen-file :private-key cmd-array name)))





;;;; General

(defn gen-certificate-signing-request [& {:keys [key-file passin passout config subject cn name ]
                                          :or {key-file "private-key.pem"
                                               cn "localhost"
                                               name "cert.csr"}}]
  (let [cmd-array (cond-> ["openssl" "req" "-new"]
                    config (concat ["-config" config])
                    key-file (concat ["-key" (work-dir key-file)])
                    passin (concat ["-passin" (str "pass:" passin)])
                    passout (concat ["-passout" (str "pass:" passout)])
                    :exec (concat ["-subj" (or subject (str "/C=JP/ST=Tokyo/L=Chiyoda/O=OrgX/OU=IT/CN=" cn))]))]
    (gen-file :certificate-request cmd-array name)))



(defn self-certify [& {:keys [key-file csr-file passin days name]
                       :or {key-file "private-key.pem"
                            days 365
                            name "x509.crt"
                            csr-file "cert.csr"}}]
  
  (let [cmd-array (cond-> ["openssl" "x509" "-req"]
                                 key-file (concat ["-signkey" (work-dir key-file)])
                                 days (concat ["-days" (str days)])
                                 csr-file (concat ["-in" (work-dir csr-file)])
                                 :always (concat ["-passin" (str "pass:" passin)]))]
    (gen-file :self-certificate cmd-array name)))




;;;; CA

(defn gen-ca-cert [& {:keys [days ca-root ca-config-path key-file subject cn name]
                      :or {cn "localhost"
                           name "ca-cert.pem"
                           key-file "private-key.pem"}}]
  (let [cmd-array (cond-> ["openssl" "req" "-new" "-x509"]
                    days (concat ["-days" days])
                    ca-config-path (concat ["-config" ca-config-path])
                    key-file (concat ["-key" (work-dir key-file)])
                    :always (concat ["-subj" (or subject (str "/C=JP/ST=Tokyo/L=Chiyoda/O=OrgX/OU=CA/CN=" cn))]))]
    (gen-file :ca-cert cmd-array name)))

(defn ca-certify [& {:keys [ca-config-path csr-file days ca-key-file ca-cert-file name san]
                     :or {name "x509.crt"
                          csr-file (str "cert.csr")}}]
  (let [san-file (str "san-" (gensym))
        cmd-array (cond-> ["openssl" "ca"]
                    ca-key-file (concat ["-keyfile" ca-key-file])
                    ca-cert-file (concat ["-cert" ca-cert-file])
                    csr-file (concat ["-in" (work-dir csr-file)])
                    days (concat ["-days" (str days)])
                    san (concat ["-extfile" san-file])
                    :always (concat ["-config" (or ca-config-path (ca-dir "ca.cnf"))])
                    :always (concat ["-notext"])
                    :always (concat ["-batch"]))]
    (with-sh-env {:CA_DIR (or *ca-root* (fs/parent ca-config-path))}
      (spit san-file (str "subjectAltName = " san))
      (gen-file :certificate cmd-array name))
    (when (.exists (io/file san-file)) (fs/delete san-file))))

;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Sequence
;;; Use this function to create self cert with one stop
(defn gen-self-certification [& {:keys [cn passkey] :as opts}]
  (gen-private-key :passout passkey)
  (gen-certificate-signing-request :cn cn :passin passkey)
  (self-certify :passin passkey))



;; X509
(defn x509->pkcs12 [& {:keys [in-file key-file passin passout alias p12-out]
                       :or {in-file "x509.crt"
                            key-file "private-key.pem"}}]
  (let [p12-out (or p12-out (->> (-> (str/split in-file #"\.")
                                     (butlast )
                                     (str/join)
                                     (str ".p12"))))
        cmd-array (cond-> ["openssl" "pkcs12" "-export"]
                    in-file (concat ["-in" (work-dir in-file)])
                    key-file (concat ["-inkey" (work-dir key-file)])
                    alias (concat ["-name " alias])
                    :exec (concat ["-out" (work-dir p12-out)])
                    :exec  (concat ["-passin" (str "pass:" passin)])
                    :exec  (concat ["-passout" (str "pass:" passout)]))]
    (if *dry*
      (println cmd-array)
      (let [_ (debug cmd-array)
            {:keys [exit out err]} (apply sh cmd-array)]
        (if (= 0 exit)
          (println "created pkcs12:" p12-out)
          (println err))))))


(defn gen-self-certification-pkcs12 [& {:keys [passkey store-pass] :as args}]
  ;; Java does not support the difference of keypass and storepass.
  (apply gen-self-certification args)
  (x509->pkcs12 :passin passkey :passout (or store-pass passkey)))

;; PKCS12

(defn pkcs12->x509 [& {:keys [passin passout p12-file keys? certs? cacerts?]}]
  (let [cmd-array (cond-> ["openssl" "pkcs12" "-nodes"]
                    p12-file (concat ["-in" (work-dir p12-file)])
                    (some? keys?) (concat ["-nocerts"])
                    (some? certs?) (concat ["-nokeys"])
                    (some? cacerts?) (concat ["-cacerts" "-nokeys" "-nocerts"])
                    :exec  (concat ["-passin" (str "pass:" passin)])
                    :exec  (concat ["-passout" (str "pass:" passout)])
                    :exec (concat ["-info"]))]
    
    (if *dry*
      (println (str/join " " cmd-array))
      (let [{:keys [exit out err]} (apply sh cmd-array)]
        (if (= 0 exit)
          out
          (println err))))))

(defn inspect [pem-path & {:keys [passin passout passkey]}]
  (let [suffix (first (re-find #"(...)$" pem-path))
        cmd-array (cond-> ["openssl"]
                    (#{"crt"} suffix) (conj "x509")
                    (#{"csr"} suffix) (conj "req")
                    (#{"pem"} suffix) (conj "rsa")
                    (#{"p12" "pkcs12"} suffix) (conj "pkcs12")
                    (not (#{"p12" "pkcs12"} suffix)) (conj "-text") ;; Not support -text in pkcs12
                    true (conj (str "-in " pem-path))
                    passin (conj (str "-passin pass:" passin))
                    passout (conj (str "-passout pass:" passout))
                    passkey (conj (str "-passout pass:" passkey) (str "-passin pass:" passkey))
                    (#{"crt"} suffix) (conj "-pubkey")
                    true (->> (str/join " "))
                    true (str/split #" "))
        {:keys [out err exit]} (apply sh cmd-array)]
    (debug cmd-array)
    (if (= 0 exit)
      (println out)
      (println err))))

(defn ez-cert [& {:keys [cn subject ca-dir passin passout] :or {ca-dir "CA"}}]
  (when-not cn
    (println "No CN found. Use localhost"))

  (if (or (and ca-dir (not (.exists (io/file ca-dir "ca.cnf"))))
          (not (.exists (io/file ca-dir "ca.cnf"))))
    (println "ca.cnf not found")
    (let [cn (or cn "localhost")]
      (gen-private-key :passout passout)
      ;; Need to pass 'passout' to get privatekey 
      (if subject
        (gen-certificate-signing-request :subject cn :passin passout)
        (gen-certificate-signing-request :cn cn :passin passout))
      (ca-certify :san (str "DNS:" cn) :ca-config-path (str (io/file ca-dir "ca.cnf"))))))


;; keytool -genkey -keyalg RSA -alias <key-alias> -keystore <keystore-name>.jks
;; keytool -export -alias <key-alias> -keystore <keystore-name>.jks -file <certificate-name>.cer

(def default-ca-cnf
  "# This definition stops the following lines choking if HOME isn't
# defined.
HOME                    = .
RANDFILE                = $ENV::HOME/.rnd

## Added
CA_DIR                  = ca

# Extra OBJECT IDENTIFIER info:
#oid_file               = $ENV::HOME/.oid
oid_section             = new_oids

[ new_oids ]
# Policies used by the TSA examples.
tsa_policy1 = 1.2.3.4.1
tsa_policy2 = 1.2.3.4.5.6
tsa_policy3 = 1.2.3.4.5.7

####################################################################
[ ca ]
default_ca      = CA_default            # The default ca section


[ CA_default ]
dir             = $ENV::CA_DIR            # Where everything is kept
certs           = $dir/certs            # Where the issued certs are kept
database        = $dir/index.txt        # database index file.
                                        # several certs with same subject.
new_certs_dir   = $dir/certs            # default place for new certs.
certificate     = $dir/certs/ca-cert.pem       # The CA certificate
serial          = $dir/serial           # The current serial number
crlnumber       = $dir/crlnumber        # the current crl number
                                        # must be commented out to leave a V1 CRL
private_key     = $dir/ca-key.pem # The private key

name_opt        = ca_default            # Subject Name options
cert_opt        = ca_default            # Certificate field options

default_days    = 365                   # how long to certify for
default_crl_days= 30                    # how long before next CRL
default_md      = sha256                # use SHA-256 by default
preserve        = no                    # keep passed DN ordering
policy          = policy_match

# For the CA policy
[ policy_match ]
countryName             = match
stateOrProvinceName     = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

####################################################################
[ req ]
default_bits            = 2048
default_md              = sha256
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
attributes              = req_attributes
x509_extensions = v3_ca # The extentions to add to the self signed cert

[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = JP
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = Tokyo
localityName                    = Locality Name (eg, city)
localityName_default            = Tokyo
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = Muleys
organizationalUnitName          = Organizational Unit Name (eg, section)
commonName                      = Common Name (eg, your name or your server\\'s hostname)
commonName_max                  = 64
emailAddress                    = Email Address
emailAddress_max                = 64

[ req_attributes ]
challengePassword               = A challenge password
challengePassword_min           = 4
challengePassword_max           = 20
unstructuredName                = An optional company name


[ v3_req ]
# Extensions to add to a certificate request
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment

[ v3_ca ]
# Extensions for a typical CA
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:true

[ crl_ext ]
# issuerAltName=issuer:copy
authorityKeyIdentifier=keyid:always")

(defn build-ca* [& {:keys [opts args]}]
  (let [[_ root-dir] args
        {:keys [subject]} opts]
    (binding [*work-dir* (or root-dir "CA")]
      (io/make-parents (work-dir "."))
      (let [index-file (work-dir "index.txt")
            serial-file (work-dir "serial")
            certs-dir (work-dir "certs" ".keep")
            ca-conf-path (io/file (work-dir  "ca.cnf"))]
        (io/make-parents certs-dir)    
        (spit serial-file "01")
        (spit index-file "")
        (io/copy default-ca-cnf ca-conf-path)
        (with-sh-env {:CA_DIR *work-dir*}
          (gen-private-key :name "ca-key.pem")
          (gen-ca-cert :name "certs/ca-cert.pem"
                       :ca-config-path (str ca-conf-path)
                       :key-file "ca-key.pem"
                       :subject (or subject "/C=JP/ST=Tokyo/L=Chiyoda/O=CA/OU=CA/CN=CA")))))))

(defn gen-cert* [{:keys [args opts]}]
  (let [{:keys [cn ca-dir passkey self-cert pkcs12]} opts]
    (cond->  true
      self-cert (do (gen-self-certification opts))
      (not self-cert) (do (ez-cert (merge opts {:passin passkey :passout passkey})))
      ;; passin is used as 'passout' in private key
      pkcs12 (do (x509->pkcs12 :passin passkey :passout passkey)))))





(def specs [["-h" "--help" "This help"]
            ["-d" "--debug" "Debug"]
            ["-k" "--pkcs12" "Create a certification as PKCS12"]
            ["-C" "--ca-dir <path>" "CA config dir"]
            ["-s" "--self-cert" "Create a self certified certification"]])

(defn usage [summary]
  (println (->> ["Usage:"
                 ""
                 " - build-ca [ca-dir]"
                 " - gen-cert [options]"
                 " - inspect <file> [options]"
                 ""
                 "Command options: (+: arguments, =: keyval, -: hyphen options)"
                 ""
                 " - build-ca:"
                 "    + ca-dir: CA root directory to be created (default: ./CA)"
                 "    = subject: Subject (default: /C=JP/ST=Tokyo/L=Chiyoda/O=CA/OU=CA/CN=CA)"
                 ""
                 " - gen-cert:"
                 "    = cn"
                 "    = subject"
                 "    = alias"
                 "    - passkey : Use as passout of private key and store"
                 "    - pkcs12"
                 "    - self-cert"
                 "    - ca-dir"
                 ""
                 " - inspect:"
                 "    + cert-or-pem-or-pkcs12-file"
                 "    * passkey : Password for Private Key and Store (passin/passout)"
                 "    * passin : Password for store key"
                 "    * passout : Password for private key"
                 ""
                 ""
                 "hyphen options:"
                 summary
                 ""]
                (str/join \newline))))



(defn set-default-work-dir! [& id]
  (alter-var-root #'*work-dir* (constantly "tls-keys-" (or id "000"))))

(defn main [& args]
  (let [{:keys [arguments options summary]} (parse-opts args specs)
        [op & cmd-args] args
        merged-opts (util/merge-options arguments options)]
    (if (:help options)
      (usage summary)
      (try
        (proc/exec "which" "openssl")
        (binding [*work-dir* (str "tls-keys-" (rand-int 1000))]
          (when (:debug options) (debug!))
          (condp get (keyword op)
            #{:build-ca :ca}  (build-ca* merged-opts)
            #{:gen-cert} (gen-cert* merged-opts)
            #{:inspect} (if-let [f (first cmd-args)]
                          (inspect f (:opts merged-opts))
                          (throw (ex-info "No file specified" {}))
                          )
            (usage summary)))
        (catch clojure.lang.ExceptionInfo e (usage summary))
        (catch  Exception e (println e))))))





