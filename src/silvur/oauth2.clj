(ns silvur.oauth2
  (:gen-class)
  (:require [silvur.oauth2.core :refer [run-oauth2-authorization-code config* server*]]
            [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as log]
            [clojure.string :as str]
            [silvur.nio :as nio]
            [silvur.oauth2.x]
            [silvur.oauth2.idp]
            [silvur.oauth2.azure]
            [mount.core :as mount :refer [defstate ]]))

(def cli-options
  [["-H" "--host <host>" "Listen host"
    :default-desc "0.0.0.0"]
   ["-p" "--port <PORT>" "Listen port"
    :default-desc "9180"
    :parse-fn parse-long]
   ["-c" "--config-file <FILE>" "Configuration file"
    :validate [nio/exists? "No file"]]
   ["-a" "--auth-uri <URI>" "Authentication URI"]
   ["-t" "--token-uri <URI>" "Token URI"]
   ["-u" "--redirect-uri <URI>" "Redirect URI"]
   ["-i" "--client-id <ID>" "Client ID"]
   ["-s" "--client-secret <SECRET>" "Client secret"]
   ["-S" "--scope <SCOPE>" "Scopes as openid,email,profile"
    :default [:openid]
    :parse-fn #(str/split % #",")]
   ["-r" "--root-dir <Root directory>"
    :default "."]
   ["-N" "--nrepl" "Boot nREPL"
    :default false]
   [nil "--nrepl-port <PORT>" "nREPL Listen port"
    :default 7999
    :parse-fn parse-long]
   ["-d" "--debug <LEVEL>" "Debug Level"
    :default :info
    :parse-fn keyword
    :validate [#{:trace :debug :info :warn :error :fatal :report} "Should be report/fatal/error/warn/info/debug/trace"]]
   ["-h" "--help" "This help"
    :default false]])



(defn usage [summary]
  (->> ["Usage: slv oauth2 <listen|client> [options]" 
        ""
        "sub command:"
        " * sample <x|azure>: Show sample config"
        ""
        "options:"
        ""
        summary
        ""]
      (str/join \newline)))



(defn internal-cli* [{:keys [options arguments summary errors]}]
  (let [opts (-> (dissoc options :help)
                 (update :options update-keys {:host :ip}))
        [op saas] arguments]
    (cond 
      (or (:help options) (nil? op)) (do (println (usage summary))
                                         (System/exit 0))
      (not-empty errors) (println (str/join "\n" errors))
      (= "sample" op) (println (with-out-str
                                 (println)
                                 (clojure.pprint/pprint
                                  (try
                                    (deref (resolve (symbol (str "silvur.oauth2." saas "/sample-config"))))
                                    (catch Exception e (log/warn "No specified config. The sample for X or Azure are available."))))))
      (#{"cli" "client"} op) (run-oauth2-authorization-code opts)
      :else (do
              (-> (mount/with-args opts)
                  (mount/only [#'config* #'server*])
                  (mount/start))
              (println "== Context: " )
              (println (with-out-str (clojure.pprint/pprint config*)))))))

(defn main [& args]
  (let [{:keys [options arguments summary errors] :as parsed-args} (parse-opts args cli-options)]
    (log/set-level! (:debug options))
    (log/debug options)
    (internal-cli* parsed-args)))

(defn dev []
  (main "oauth2" "listen" "-c" "idp.edn"))


(comment
  @(http/post "https://idp-rt.us-east-1.anypoint.mulesoft.com/api/v1/organizations/cc4f2ae5-cfd6-4259-ac6c-62a8ac96b62d/actions/fb4c66dd-9a08-46b4-b6b1-844cf8628207/versions/1.0.0/executions" 
              {:headers {"Content-Type" "multipart/form-data" "Authorization" "Bearer d67fa81e-d12d-475c-9442-962411d57cba"} 
               :multipart [{:name "file" :content (io/file "/home/myst/public/invoice-sample.pdf") :filename "invoice-sample.pdf"}
                           {:name "callback" :content (edn->json {:noAuthUrl "http://theorems.io:9180/dump"})}]}))


;; @(http/post "https://proxy-jsonplaceholder-api-49wz6g.fmxi3r-2.jpn-e1.cloudhub.io/todos/1"
;;   {:headers {"Authorization" "Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3RoZW9yZW1zLmlvIiwiYXVkIjoibXljbGllbnQiLCJleHAiOjE2OTYxNzc1OTB9.l66Tvyi4e1-YEIRYgC6WUAIMWY19UOLUoGlIigghmjc"}})



;;123456

;; http://theorems.io:9180/oauth2/connect/register
;; http://theorems.io:9180/oauth2/auth
;; http://theorems.io:9180/oauth2/token
;; http://theorems.io:9180/oauth2/connect/introspect




