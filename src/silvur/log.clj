(ns silvur.log
  (:require [taoensso.timbre :as log]
            [silvur.util :refer [edn->json json->edn]]
            [clojure.data.json :as json]

            [clojure.string :as str]))

(defmacro debug [& xs]
  `(log/debug ~@xs))

(defmacro info [& xs]
  `(log/info ~@xs))

(defmacro warn [& xs]
  `(log/warn ~@xs))

(defmacro error [& xs]
  `(log/error ~@xs))

(defmacro trace [& xs]
  `(log/trace ~@xs))

(defmacro set-min-level! [level]
  `(log/set-min-level! ~level))

(defmacro spy [& args]
  `(log/spy ~@args))

(defn get-config []
  log/*config*)




