(ns silvur.cli
  (:gen-class)
  (:require [clojure.tools.cli :refer (parse-opts)]
            [clojure.string :as str]
            [silvur.http :as http]
            [silvur.openssl :as ssl]
            [silvur.oauth2 :as oauth2]))

(def specs [["-h" "--help"]
            ["-v" "--version"]])

(def version "3.4.0")

(defn usage [summary]
  (->> ["Usage: slv <sub command> [options]" 
        ""
        "sub commands:"
        ""
        " - tls     <build-ca|gen-cert|inspect> [options]"
        " - oauth2  <listen|sample> [options]"
        " - jwt "
        " - http    <listen> [options]"
        ""
        "global options:"
        summary
        ""
        ]
      (str/join \newline)))

(defn -main [& args]
  (let [{:keys [options arguments summary]} (parse-opts args specs)
        [sub-cmd & _] arguments]
    (if (:version options)
      (println "version:" version)
      (cond
        (#{"tls" "ssl"} sub-cmd) (apply ssl/main (rest args))
        (#{"oauth2" "oauth"} sub-cmd) (apply oauth2/main (rest args))
        (#{"http"} sub-cmd) (apply http/main (rest args))
        :else  (println (usage summary)))))
  (shutdown-agents))
