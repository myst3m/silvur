;; *   Silvur
;; *
;; *   Copyright (c) Tsutomu Miyashita. All rights reserved.
;; *
;; *   The use and distribution terms for this software are covered by the
;; *   Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
;; *   which can be found in the file epl-v10.html at the root of this distribution.
;; *   By using this software in any fashion, you are agreeing to be bound by
;; * 	 the terms of this license.
;; *   You must not remove this notice, or any other, from this software.

(ns silvur.datetime
  (:gen-class)
  (:import [java.util Date GregorianCalendar]
           [java.time.format DateTimeFormatter DateTimeParseException]
           [java.time LocalTime Period DayOfWeek
            ZonedDateTime ZoneOffset ZoneId Instant Duration ZoneRegion LocalDateTime
 ]
           [java.time.temporal ChronoUnit ChronoField TemporalAdjuster TemporalAdjusters
            Temporal TemporalUnit TemporalAmount]))

;; Compliant to ISO-8601
;; Monday: 1
;; Sunday: 7

;; Minimum unit is 'second'
(def UTC (ZoneId/of "UTC"))
(def JST (ZoneId/of "Asia/Tokyo"))
(def NYC (ZoneId/of "America/New_York"))
(def CET (ZoneId/of "Europe/Berlin"))

(def FORMAT {:JP "YYYY/MM/dd HH:mm:ss"
             :US "MM/dd/YYYY HH:mm:ss"})

(declare datetime)
(declare inst<)

(defonce ^:dynamic *tz* JST)
(defonce ^:dynamic *precision* (ChronoUnit/MILLIS)) 

(defprotocol TimeExchange
  (minutes-of-week [t])
  (first-date-of-week [t])
  (adjust [t duration])
  (-day [i])
  (-hour [i])
  (-minute [i])
  (-second [i])
  (before [i diff])
  (after [i diff])
  (-year [t])
  (-month [t])
  (-week [t])
  (+time [t duration])
  (-inst< [t tz]))

(defprotocol TimeCompare
  (before? [s t])
  (before=? [s t])
  (after? [s t])
  (after=? [s t]))

(defprotocol Zoning
  (-at-zone [t zid])
  (-at-offset [t zid])
  (with-zone [t tz]))

(extend-protocol Zoning
  Instant
  (-at-zone [^Instant t ^ZoneId zid]
    (.atZone t zid))
  (at-offset [^Instant t ^ZoneId zid]
    (.atOffset t (-> ^ZoneId zid (.getRules) (.getOffset t))))
  (with-zone [^Instant t ^ZoneId zid]
    (-> (.atZone t *tz*)
        (.withZoneSameInstant zid)))
  
  LocalDateTime
  (-at-zone [^LocalDateTime t ^ZoneId zid]
    (.atZone t zid))
  (-at-offset [^LocalDateTime t ^ZoneId zid]
    (.atOffset t (-> ^ZoneId zid (.getRules) (.getOffset t))))
  (with-zone [^LocalDateTime t ^ZoneId tz]
    (-> (.atZone t ^java.time.ZoneId *tz*)
        (.withZoneSameInstant tz)))
  
  ZonedDateTime
  (-at-zone [^ZonedDateTime t ^ZoneId tz]
    (.withZoneSameLocal t tz))
  (with-zone [^ZonedDateTimet t ^ZoneId tz]
    (.withZoneSameInstant t tz)))

(extend-protocol TimeCompare
  Instant
  (before? [s t]
    (.isBefore s t))
  (after? [s t]
    (.isAfter s t))
  (before=? [s t]
    (or (.isBefore s t) (.isEqual s t)))
  (after=? [s t]
    (or (.isAfter s t) (.isEqual s t)))

  LocalDateTime
  (before? [s t]
    (.isBefore s t))
  (after? [s t]
    (.isAfter s t))
  (before=? [s t]
    (or (.isBefore s t) (.isEqual s t)))
  (after=? [s t]
    (or (.isAfter s t) (.isEqual s t)))
  
  ZonedDateTime
  (before? [s t]
    (.isBefore s t))
  (after? [s t]
    (.isAfter s t))
  (before=? [s t]
    (or (.isBefore s t) (.isEqual s t)))
  (after=? [s t]
    (or (.isAfter s t) (.isEqual s t))))

(defn at-zone
  ([t]
   (-at-zone t *tz*))
  ([t zid]
   (-at-zone t zid)))

(defn at-offset
  ([t]
   (-at-offset t *tz*))
  ([t zid]
   (-at-offset t zid)))

(defn chrono-unit [x]
  (case x
    :days (ChronoUnit/DAYS)
    :hours (ChronoUnit/HOURS)
    :minutes (ChronoUnit/MINUTES)
    :seconds (ChronoUnit/SECONDS)
    :millis (ChronoUnit/MILLIS)
    :nanos (ChronoUnit/NANOS)))

(defn set-default-precision! [x]
  (alter-var-root #'*precision* (constantly (chrono-unit x))))

(defn set-default-tz! [tz]
  (alter-var-root #'*tz* (constantly tz)))

(defn zone-offset
  ([]
   (zone-offset *tz* (datetime)))
  ([^ZoneId zid ^LocalDateTime ldt]
   (-> ^ZoneId zid (.getRules) (.getOffset ldt))))

(defn inst> [^Instant i]
  (let [d (.plus (.multipliedBy (.getDuration (ChronoUnit/NANOS)) (.getNano i))
                 (.multipliedBy (.getDuration (ChronoUnit/SECONDS)) (.getEpochSecond i)))] 
    (condp = (str *precision*)
      "Seconds" (.getSeconds d)
      "Millis" (.toMillis d)
      "Nanos" (.toNanos d)
      (throw (ex-info "No support" {:precision *precision*})))))


(defmulti -datetime (fn [arg & _]  (class arg)))

(defmethod -datetime Integer [arg & rest]
  (apply -datetime (long arg) rest))

(defmethod -datetime Long [arg & rest]
  (if (and (>= 9999 arg 1970)
           (not (instance? clojure.lang.Keyword (first rest))))
    (LocalDateTime/of arg (nth rest 0 1) (nth rest 1 1) (nth rest 2 0) (nth rest 3 0) (nth rest 4 0) (nth rest 5 0))
    (binding [*precision* (chrono-unit (first rest))]
      (LocalDateTime/ofInstant (inst< arg) *tz*))))

(defmethod -datetime java.time.format.DateTimeFormatter [arg & [datetime-str]]
  (if datetime-str
    (LocalDateTime/parse datetime-str arg)
    (LocalDateTime/now)))

(defmethod -datetime String [datetime-str & [datetime-fmt]]
  ;; arg is expected as a format like "yyyy-MM-dd HH:mm:ss"
  (-datetime (if datetime-fmt
               (DateTimeFormatter/ofPattern datetime-fmt)
               (DateTimeFormatter/ISO_LOCAL_DATE_TIME))
             datetime-str))

(defmethod -datetime clojure.lang.Keyword [arg & rest]
  (.format ^LocalDateTime (apply -datetime rest) (condp = arg
                                                   :default (DateTimeFormatter/ISO_LOCAL_DATE_TIME)
                                                   :basic-iso-date (DateTimeFormatter/BASIC_ISO_DATE)
                                                   :iso-date (DateTimeFormatter/ISO_DATE)
                                                   :iso-date-time (DateTimeFormatter/ISO_DATE_TIME)
                                                   (DateTimeFormatter/ISO_INSTANT))))


(defmethod -datetime ZonedDateTime [arg & rest] (.toLocalDateTime arg))

(defmethod -datetime LocalDateTime [arg & rest] arg)

(defmethod -datetime clojure.lang.LazySeq [arg & _]
  (map -datetime arg))

(defmethod -datetime clojure.lang.PersistentVector [arg & _]
  (apply -datetime arg))

(defmethod -datetime Date [arg & rest]
  (-datetime (.getTime ^Date arg) :millis))

(defmethod -datetime clojure.lang.PersistentArrayMap [arg & _]
  (-datetime (or (:datetime arg)
                 (:time arg)
                 (:t arg))))

(defmethod -datetime clojure.lang.PersistentHashMap [arg & _]
  (-datetime (or (:datetime arg)
                 (:time arg)
                 (:t arg))))


(defmethod -datetime Instant [arg & _]
  (-> (.atZone  arg  *tz*)
      (.toLocalDateTime)))

(defmethod -datetime :default [arg & rest]
  (LocalDateTime/now))


(defn datetime
  ([]
   (-> (LocalDateTime/now)
       (.truncatedTo *precision*)))
  ([arg & rest]
   (apply -datetime arg (->> rest vector (keep identity) flatten))))

(defn datetime*
  ([]
   (datetime* (LocalDateTime/now ^ZoneId *tz*)))
  ([arg & rest]
   (-> (apply datetime arg rest)
       ^java.time.OffsetDateTime
       (at-offset)
       ^java.time.OffsetDateTime
       (.toInstant)
       (inst>))))


(defn date<
  ([]
   (date< (datetime)))
  ([^LocalDateTime ldt]
   (Date/from (.toInstant (at-offset ldt)))))

(defn vec< [^LocalDateTime ldt]
  [(.getYear ldt) (.getMonthValue ldt) (.getDayOfMonth ldt)
   (.getHour ldt) (.getMinute ldt) (.getSecond ldt)])


(extend-protocol TimeExchange
  Long
  (-year [i]
    (Period/ofYears i))
  (-month [i]
    (Period/ofMonths i))
  (-week [i]
    (Period/ofWeeks i))
  (-day [i]
    (Period/ofDays i))
  (-hour [i]
    (Duration/ofHours i))
  (-minute [i]
    (Duration/ofMinutes i))
  (-second [i]
    (Duration/ofSeconds i))
  (adjust [v duration-or-period]
    (-> (datetime v)
        (.truncatedTo (proxy [TemporalUnit] []
                        (getDuration []
                          duration-or-period)))
        (datetime*)))
  (+time [v duration-or-period]
    (datetime* (+time (datetime v) duration-or-period)))
  (first-date-of-week [i]
    (first-date-of-week (datetime i)))
  (minutes-of-week [i]
    (minutes-of-week (datetime i)))
  (-inst< [t tz]
    (-> ^ChronoUnit
        *precision*
      (.getDuration )
      (.multipliedBy  t)
      (.addTo (Instant/EPOCH))))


  ;; LocalDatetime
  java.time.LocalDateTime
  (-year [this]
    (.getYear this))
  (-month [this]
    (.getMonthValue this))
  (-hour [this]
    (.getHour this))
  (-minute [this]
    (.getMinute this))
  (-second [this]
    (.getSecond this))
  (-inst< [t tz]
    (.toInstant t (zone-offset tz t)))
  (adjust [t duration-or-period]
    (condp instance? duration-or-period
      TemporalAdjuster (.with t duration-or-period)
      Duration (.truncatedTo t (proxy [TemporalUnit] []
                                  (getDuration []
                                    duration-or-period)))
      Period (.with t (proxy [TemporalAdjuster] []
                        (adjustInto [z]
                          (.truncatedTo (.subtractFrom duration-or-period z)
                                        (proxy [TemporalUnit] []
                                          (getDuration []
                                            (-hour 24)))))))
      (throw (ex-info "Not duration or period" {}))))
  
  (+time [t duration-or-period]
    (.plus t duration-or-period))
  (minutes-of-week [t]
    (+
     (* (.getMinute t))
     (* 60 (.getHour t))
     (* 60 24 (-> t (.getDayOfWeek) (.getValue) dec))))
  
  (first-date-of-week [t]
    (-> (adjust t (DayOfWeek/MONDAY))
        (.truncatedTo (ChronoUnit/DAYS))))

  (before [t duration-or-period]
    (.minus t duration-or-period))

  (after
    (^java.time.LocalDateTime [^java.time.LocalDateTime t duration-or-period]
     (.plus t duration-or-period)))

  ;; ZonedDateTime
  java.time.ZonedDateTime
  (-year [t]
    (.getYear t))
  (-month [t]
    (.getMonthValue t))
  (-day [^java.time.LocalDateTime t]
    (.getDay t))
  (-hour [t]
    (.getHour t))
  (-minute [t]
    (.getMinute t))
  (-second [t]
    (.getSecond t))
  (-inst< [t tz]
    (.toInstant t))) 

(defn inst<
  ([t]
   (inst< t *tz*))
  ([t tz]
   (-inst< t tz)))

(defn day
  ([] (-day 1)) 
  ([i] (-day i)))
(defn hour
  ([] (-hour 1)) 
  ([i] (-hour i)))
(defn minute
  ([] (-minute 1)) 
  ([i] (-minute i)))

(defn until 
  ([^Temporal t]
   (until t (datetime)))
  ([^Temporal t0 ^Temporal t1]
   (.until (datetime t0) (datetime t1) *precision*)))

(defn between
  ([^Temporal t0 ^Temporal t1]
   (between *precision* t0 t1))
  ([^TemporalUnit temporal-unit ^Temporal t0 ^Temporal t1]
   (.between temporal-unit t0 t1)))

(defn today []
  (.truncatedTo (datetime) (ChronoUnit/DAYS)))

(defn yesterday []
  (adjust (datetime) (day)))

(defn tomorrow []
  (-> (datetime)
      (after (day))
      (.truncatedTo (ChronoUnit/DAYS))))

(defn time-seq
  ([delta]
   (time-seq (datetime) delta))
  ([t delta]
   (lazy-seq
    (cons (adjust t delta) (time-seq (before t delta) delta)))))

