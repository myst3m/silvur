(ns silvur.nio
  (:require [clojure.java.io :as io])
  (:import [java.io File]
           [java.nio.charset Charset]
           [java.util.stream Collectors]
           [java.nio.file Path Paths Files LinkOption OpenOption StandardCopyOption]
           [java.nio.file.attribute FileAttribute]
           [java.util.concurrent LinkedBlockingQueue TimeUnit]))

(extend Path
  io/IOFactory
  {:make-reader (fn [path {:keys [encoding] :or {encoding "UTF-8"}}]
                  (Files/newBufferedReader path (Charset/forName encoding)))
   :make-writer (fn [path opts]
                  (Files/newBufferedWriter path (into-array OpenOption opts)))
   :make-input-stream (fn [^Path path opts] (io/make-input-stream (.toFile path) opts))
   :make-output-stream (fn [^Path path opts] (io/make-output-stream (.toFile path) opts))})

(defprotocol PathOperation
  (-path [f]))

(extend-protocol PathOperation
  String
  (-path [s] (.toPath (io/file s)))
  File
  (-path [f] (.toPath f))
  Path
  (-path [p] p))

(defn path [p & ps]
  (-path (apply io/file (cons p ps))))

(defn pwd []
  (.toAbsolutePath ^Path (path "")))

(defn exists? [a-path]
  (Files/exists (path a-path) (into-array LinkOption [])))

(defn absolute-path [a-path]
  (.toAbsolutePath ^Path (path a-path)))

(defn readable? [a-path]
  (Files/isReadable (path a-path)))

(defn mkdir [a-path]
  (Files/createDirectories (path a-path) (into-array FileAttribute [])))

(defn directory? [a-path]
  (Files/isDirectory (path a-path) (into-array LinkOption [])))

(defn rm [a-path]
  (Files/deleteIfExists (path a-path)))

(defn path-seq [a-path]
  (-> (Files/list (path a-path))
      (.collect (Collectors/toList))))

(defn size [a-path]
  (Files/size (path a-path)))

(defn basename [a-path]
  (.getFileName (path a-path)))

(defn copy
  ([src-path dst-path]
   (copy src-path dst-path
         (into-array java.nio.file.CopyOption [StandardCopyOption/REPLACE_EXISTING])))
  ([src-path dst-path opts]
   (Files/copy (path src-path) (path (if (directory? dst-path) (str dst-path "/" (basename src-path)) dst-path)) opts)))



