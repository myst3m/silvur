;; *   Silvur
;; *
;; *   Copyright (c) Tsutomu Miyashita. All rights reserved.
;; *
;; *   The use and distribution terms for this software are covered by the
;; *   Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
;; *   which can be found in the file epl-v10.html at the root of this distribution.
;; *   By using this software in any fashion, you are agreeing to be bound by
;; * 	 the terms of this license.
;; *   You must not remove this notice, or any other, from this software.

(ns silvur.util
  (:gen-class)
  (:require [clojure.string :as str :refer [upper-case lower-case]]
            [clojure.core.async :refer (chan put! alts! go-loop timeout close!)]
            [org.httpkit.client :as http]
            [org.httpkit.server :as hs]
            [muuntaja.core :as m]
            [reitit.ring :as ring]
            [reitit.http :as rh]
            [reitit.interceptor.sieppari :as sieppari]
            [reitit.http.interceptors.parameters :as params]
            [reitit.http.interceptors.muuntaja :as muuntaja]
            [clojure.data.xml :as xml]
            [clojure.java.io :as io]
            [postal.core :as postal]
            [nrepl.server :as n]
            [nrepl.core :refer (message client)]
            [nrepl.transport :as transport]
            [taoensso.timbre :as log]
            [jsonista.core :as json]
            [camel-snake-kebab.core :as csk]
            [camel-snake-kebab.extras :as cske]
            [cognitect.transit :as transit]
            [clojure.java.shell :refer [sh]])
  (:import [java.security MessageDigest]
           [java.util.concurrent BlockingQueue LinkedBlockingQueue SynchronousQueue TimeUnit]
           [java.io File EOFException PushbackInputStream]
           [java.util.stream Collectors]
           [java.util.concurrent LinkedBlockingQueue TimeUnit]
           [java.net Socket]))

(defmacro uuid
  ([]
   `(random-uuid))
  ([s]
   `(java.util.UUID/fromString ~s)))

(defprotocol NumberConvert
  (str->int [x])
  (str->long [x])
  (str->double [x])
  (hex->long [x])
  (chars->hex [xs])
  (chars->long [xs]))

(extend-protocol NumberConvert
  java.lang.String
  (str->int [x] (Integer/valueOf x))
  (str->long [x] (Long/valueOf x))
  (str->double [x] (Double/valueOf x))
  (hex->long [x] (Long/valueOf x 16))
  (chars->hex [xs] (str/join (map #(Long/toHexString (long %)) xs)))
  (chars->long [xs] (-> xs chars->hex hex->long)))


(defn sha1sum [& vals]
  (let [sha1 (MessageDigest/getInstance "SHA1")]
    (doseq [v vals]
      (.update sha1 (.getBytes (str v) "UTF-8")))
    (str/join (map #(format "%02x" %) (.digest sha1)))))

(defmacro uk [s]
  `(try (keyword (upper-case (name ~s))) (catch Exception e# nil)))

(defmacro us [s]
  `(try (upper-case (name ~s)) (catch Exception e# nil)))

(defmacro lk [s]
  `(try (keyword (lower-case (name ~s))) (catch Exception e# nil)))

(defmacro ls [s]
  `(try (lower-case (name ~s)) (catch Exception e# nil)))

(defn json->edn 
  ;; JSON String "\"string\"" can be handled, but cannot for string likes "string".
  ([x]
   (json->edn :kebab x))
  ([type x]
   (try
     (cond->> (json/read-value x)
       (= type :kebab) (cske/transform-keys csk/->kebab-case-keyword)
       (= type :camel) (cske/transform-keys csk/->camelCase)
       :else (cske/transform-keys keyword))
     (catch Exception e x))))

(defn edn->json
  ([x]
   (edn->json :camel x))
  ([type x]
   (->> x (cske/transform-keys
           (case type
             :pascal csk/->PascalCaseString
             :kebab csk/->kebab-case-string
             :camel csk/->camelCaseString
             :snake csk/->snake_case_string
             :header csk/->HTTP-Header-Case-String
             name))
        (json/write-value-as-string))))

(defn edn->xml [edn & [raw?]]
  (letfn [(parse [edn]
            (loop [m edn
                   result {}]
              (cond
                (nil? m) result
                ;; pickup 1 element
                ;; handle element
                (map? m)  (if-let [[k v] (first m)]
                            (cond
                              (.matches (name k) "^-.*") (recur (into {} (rest m)) (update result :attrs assoc (keyword (subs (name k) 1)) v))
                              :else (recur (into {} (rest m))
                                           (let [z (parse v)]
                                             
                                             (if (vector? z)
                                               {:content (mapv #(assoc % :tag k) z)}
                                               (update result :content
                                                       (comp vec conj)
                                                       (conj {:tag k} (parse v)))))))
                            result)
                (vector? m) (mapv parse m)
                :else {:content (vec (flatten [m]))})))]
    (let [xml (:content (parse edn))]
      (cond-> (if (and (vector? xml) (< 1 (count xml)))
                {:tag :root :content xml}
                xml)
        (not raw?) (xml/indent-str)))))

;; NIO



;; SMTP
(defn mail [& {:keys [from to subject host port contents] :or {host "localhost" port 25}}]
  ;; Ex. (mail :host "lebesgue" :from "analysis@theorems.co" :to ["myst3m@gmail.com"] :subject "Test" :contents {:text/plain "Hello"})
  (postal/send-message {:host host}
                       {:from from :to to :subject subject
                        :body (reduce conj [] (map (fn [[type content]]
                                                     (hash-map :type (condp = type
                                                                       :attachment type
                                                                       :inline type
                                                                       (clojure.string/replace (str type "; charset=utf-8") #":" ""))
                                                               :content content))
                                                   contents))}))


(defn parse-binary-message [format-coll data-coll]
  ;; format-coll: [1 3 3 4 4 4 4 4 4]
  ;;              [[1 chars->hex] [3 chars->hex] ....]
  (loop [sf format-coll
         pdata []
         zs data-coll]
    (let [v (first sf)
          [s cf] (cond
                   (number? v) [v]
                   (sequential? v) v
                   (= :* v) [(count zs)]
                   :else [])]
      (if-not (seq sf)
        pdata
        (recur (rest sf) (conj pdata ((or cf identity) (take s zs))) (drop s zs))))))


(defonce nrepl-server (atom nil))
(def lock (Object.))
;; nREPL


(defn nrepl-handler [{:keys [params session headers request-method] :as req}]
  
  (let [msg (clojure.walk/keywordize-keys params)
        {:strs [timeout] :or {timeout "5000"}} headers
        [rq wq :as transport] (or (::transport session)
                                       (transport/piped-transports))
        timeout (parse-long timeout)]
    (log/info msg)
    {:status 200
     :session {::transport transport ::client client}
     :headers {"Content-Type" "application/json"}
     :body (do
             (if (:op msg)
               (let [handle-fn (n/default-handler)]
                 (deref (handle-fn (-> (update msg :code #(str "(do " % ")"))
                                       (assoc :transport wq))))
                   ;; result -> [write queue] -- [read queue] -> read -> edn-data
                 (loop [m {} 
                        result (transport/recv rq timeout)]
                   (if (or (get (:status m) :done) (nil? m))
                     (edn->json result)
                     (recur (transport/recv rq timeout)
                            (update result :out str (:out m))))))
               (edn->json {:err "No op specified" :state #{:error}})))}))

(def route ["" {:muuntaja m/instance}
            ["/ping" {:get (fn [req] {:status 200 :body "pong"})}]
            ["/nrepl" {:post nrepl-handler}]])

(def app-default (rh/ring-handler
                  (rh/router route)
                  (ring/routes
                   (ring/create-default-handler))
                  {:interceptors [(muuntaja.interceptor/format-interceptor)
                                  (muuntaja.interceptor/params-interceptor)]
                   :executor sieppari/executor}))


(def ^:dynamic *options* {})

(defn nrepl-start [& {:keys [ip port cider tty app] :or {ip "0.0.0.0" port 7888} :as opts}]
  (locking lock
   (when-not @nrepl-server
     (letfn [(nrepl-handler []
               (require 'cider.nrepl)
               (ns-resolve 'cider.nrepl 'cider-nrepl-handler))]

       (println (str "Boot nREPL server on " ip  ":" port (when cider (str " with cider option"))))
       (reset! nrepl-server
               (binding [*options* opts]
                 (cond 
                   cider (n/start-server :port port :bind ip :handler (bound-fn* (nrepl-handler)))
                   tty (n/start-server :port port :bind ip
                                       :handler (bound-fn* (nrepl.server/default-handler))
                                       :transport-fn transport/tty
                                       :greeting-fn (fn [transport]
                                                      (transport/send transport
                                                                      {:out (str "\nWelcome to  nREPL !\n\n"
                                                                                 "user=> ")})))
                   :else (hs/run-server (bound-fn* (or app  #'app-default)) {:host ip :port port}))))))))

(defn nrepl-stop [& [srv]]
  (locking lock
   (when @nrepl-server
     (if (fn? @nrepl-server)
       (@nrepl-server)
       (n/stop-server (or srv @nrepl-server)))
     (reset! nrepl-server nil))))



;; To be used from Java
(defmulti nrepl (fn [type & _] (keyword (name type))))
(defmethod nrepl :start [type & args]
  (apply nrepl-start args))
(defmethod nrepl :stop [type & args]
  (nrepl-stop))


(defonce control-ports (atom {}))

(defn stop-routine
  ([]
   (put! (last (first @control-ports)) :quit))
  ([id]
   (put! (@control-ports id) :quit)))


(defn start-routine [f & [{:keys [id queue-length interval]
                           :as opts}]]
  (let [ctrl-port (chan (or queue-length 1))
        id (or id (gensym))]
    (swap! control-ports assoc id ctrl-port)
    (go-loop [id id]
      (let [[v ch] (alts! [ctrl-port (timeout (cond
                                                (fn? interval) (interval)
                                                (number? interval) interval
                                                :else 2000))])]
        (if (and (= :quit v) (= ch ctrl-port))
          (do (log/info "Go routine" id "quit")
              (close! (@control-ports id))
              (swap! control-ports dissoc id))
          (do (f opts)
              (recur id)))))
    ctrl-port))

(defn merge-options [arguments options]
  (let [eq-opts (->> (keep #(re-find #"([\w-.*]+)=([\w-.*]+)" %) arguments)
                     (mapv (comp vec rest))
                     (mapv #(update-in % [0] keyword))
                     (into {} ))
        reg-opts (filterv #(not (re-find #"([\w-.*]+)=([\w-.*]+)" %)) arguments)]
    {:args reg-opts
     :opts (merge eq-opts options)}))

;; (defn nrepl-connect
;;   ([]
;;    (nrepl-connect "localhost" 7888))
;;   ([host port]
;;    (nrepl.core/url-connect (str "transit+json://" host ":" (or port 7888)))))

;; (defn nrepl-close [conn]
;;   (when conn (.close conn)))

;; (defn nrepl-send-message [conn msg]
;;   ;; msg: {:op "eval" :code "(str (+ 1 1))"}
;;   (-> (nrepl.core/client conn 2000)
;;       (nrepl.core/message msg)))

;; (def conn (nrepl.core/url-connect "transit+json://localhost:9999"))
;; (-> (nrepl.core/client conn 2000) (nrepl.core/message {:op "eval" :code "(str (+ 1 1))"}))
