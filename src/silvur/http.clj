(ns silvur.http
  (:gen-class)  
  (:refer-clojure :exclude [get])
  (:require [silvur.util :refer :all]
            [silvur.log :as slog]
            [silvur.nio :as nio]
            [clojure.tools.cli :refer (parse-opts)]
            [silvur.datetime :refer [datetime* chrono-unit *precision*]]
            [mount.core :as mount :refer [defstate]]
            [org.httpkit.client :as http]
            [org.httpkit.server :as srv]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]
            [clojure.string :as str]
            [reitit.core :as r]
            [reitit.http :as rh]
            [clojure.pprint :as pp]
            [reitit.ring :as ring]
            [clojure.data.json :as json])
  (:import [java.net URI]
           [javax.net.ssl SNIHostName SNIServerName SSLEngine SSLParameters X509TrustManager]))

;; SSL


(defn sni-configure [^SSLEngine ssl-engine ^URI uri]
  (let [^SSLParameters ssl-params (.getSSLParameters ssl-engine)]
    (.setServerNames ssl-params [(SNIHostName. (.getHost uri))])
    (.setUseClientMode ssl-engine true)
    (.setSSLParameters ssl-engine ssl-params)))

;; When using SNI, it should make a client as below for httpkit
;; (defonce sni-client
;;   ;; SNI (Server Name Indication) is a TLS extension to multiplex multiple SSL termination on a single IP  
;;   (http/make-client {:ssl-configurer ssl/sni-configure}))

(defn engine []
  (let [tm (reify javax.net.ssl.X509TrustManager
             (getAcceptedIssuers [this] (make-array  java.security.cert.X509Certificate 0))
             (checkClientTrusted [this chain auth-type])
             (checkServerTrusted [this chain auth-type]))
        client-context (javax.net.ssl.SSLContext/getInstance "TLSv1.2")]
    (.init client-context nil
           (into-array javax.net.ssl.TrustManager [tm])
           nil)
    (doto (.createSSLEngine client-context)
        (.setUseClientMode true))))

(def STATUS
  {100 "Continue",
   101 "Switching Protocols",
   102 "Processing",
   103 "Early Hints",
   200 "OK",
   201 "Created",
   202 "Accepted",
   203 "Non-Authoritative Information",
   204 "No Content",
   205 "Reset Content",
   206 "Partial Content",
   207 "Multi-Status",
   208 "Already Reported",
   226 "IM Used",
   300 "Multiple Choices",
   301 "Moved Permanently",
   302 "Found",
   303 "See Other",
   304 "Not Modified",
   305 "Use Proxy",
   306 "Unused",
   307 "Temporary Redirect",
   308 "Permanent Redirect",
   400 "Bad Request",
   401 "Unauthorized",
   402 "Payment Required",
   403 "Forbidden",
   404 "Not Found",
   405 "Method Not Allowed",
   406 "Not Acceptable",
   407 "Proxy Authentication Required",
   408 "Request Timeout",
   409 "Conflict",
   410 "Gone",
   411 "Length Required",
   412 "Precondition Failed",
   413 "Payload Too Large",
   414 "URI Too Long",
   415 "Unsupported Media Type",
   416 "Range Not Satisfiable",
   417 "Expectation Failed",
   418 "I'm a Teapot",
   421 "Misdirected Request",
   422 "Unprocessable Entity",
   423 "Locked",
   424 "Failed Dependency",
   425 "Too Early",
   426 "Upgrade Required",
   428 "Precondition Required",
   429 "Too Many Requests",
   431 "Request Header Fields Too Large",
   451 "Unavailable For Legal Reasons",
   500 "Internal Server Error",
   501 "Not Implemented",
   502 "Bad Gateway",
   503 "Service Unavailable",
   504 "Gateway Timeout",
   505 "HTTP Version Not Supported",
   506 "Variant Also Negotiates",
   507 "Insufficient Storage",
   508 "Loop Detected",
   510 "Not Extended",
   511 "Network Authentication Required"})


(defmacro with-tracing [opts & body]
  `(binding [*precision* (chrono-unit :millis)]
     (let [s# (datetime*)
           tran-id# (symbol (format "%04d" (rand-int 9999)))]
       (log/trace :http-trace-appender ~opts 0 tran-id#)
       (let [result# (deref (do ~@body))
             e# (datetime*)]
         (log/trace :http-trace-appender result# (- e# s#) tran-id#)
         result#))))

(defn -request [{:keys [url method] :as opts} & [callback]]
  (with-tracing opts
    (http/request (assoc opts :client (http/make-client {:ssl-configurer sni-configure})) callback)))


(defn get [uri & [opts callback]]
  (-request (assoc opts :method :get :url uri) callback))

(defn post [uri & [opts callback]]
  (-request (assoc opts :method :post :url uri) callback))

(defn put [uri & [opts callback]]
  (-request (assoc opts :method :put :url uri) callback))

(defn patch [uri & [opts callback]]
  (-request (assoc opts :method :patch :url uri) callback))

(defn delete [uri & [opts callback]]
  (-request (assoc opts :method :delete :url uri) callback))

(defn head [uri & [opts callback]]
  (-request (assoc opts :method :head :url uri) callback))

(defn move [uri & [opts callback]]
  (-request (assoc opts :method :move :url uri) callback)  )

(defn http-trace-appender
  "Disable default println as (taoensso.timbre/merge-config! {:appenders {:println {:enabled? false} :http-tracer (log/http-trace-appender)}})
  And (set-min-level! :trace)
  "
  [{:keys [level]}]
  {:enabled? true
   :ns-filter #{"silvur.http"}
   :fn (let []
         (fn [{:keys [vargs]}]
           (when (= :http-trace-appender (first vargs))
             (let [[id-key {:keys [headers body multipart method url status] :as http-data} duration & [tran-id]] vargs]
               (print (if status
                        (str "<=== " [tran-id] " "  status " " (STATUS status) (when duration (str " " duration "ms") ) "\n")
                        (str "===> " [tran-id] " "  (str/upper-case (name method)) " " url "\n")))
               (flush)
               (when (< 0 level)
                 (doseq [[k v] headers]
                   (print (format  "%s: %s\n" (name k) v))
                   (flush))
                 (println)
                 (println)
                 (cond
                   ;;No body
                   (and (empty? body) (empty? multipart))
                   (do (print "--- No body ---\n")
                       (flush))
                   
                   ;; JSON
                   (and (string? body)
                        (->> headers
                             (filter (fn [[k v]]
                                       (and (= (str/lower-case (name k)) "content-type")
                                            (= (re-find #"json" v)))))
                             (first)))
                   (json/pprint-json (json/read-str body))
                   ;; Multipart
                   (some? multipart) (do (print "----- Multipart -----\n")
                                         (flush)
                                         (clojure.pprint/pprint multipart)
                                         (print "---------------------\n")
                                         (flush))
                   ;; Map
                   (map? body) (clojure.pprint/pprint body )
                   ;; Seq
                   (seq? body) (clojure.pprint/pprint body)
                   ;; Null
                   (and (nil? body) (nil? multipart)) (print "--- No body ---\n")
                   ;; else
                   :else (do (print body) (flush))) 
                 (println)
                 (println))))))})


(defn gen-app [{:keys [root-dir]}]
  (fn [{:keys [uri]}]
    (let [f (io/file root-dir (str/replace uri #"^/*" ""))]
      (if (nio/exists? f)
        {:status 200 :body f}
        {:status 404}))))

(defn app [{:keys [root-dir base-path] :as opts}]
  (rh/ring-handler
   (rh/router [["/challenge" (fn [req]
                               {:status 200 :body (slurp (:body req))})]
               
               ["/dump" (fn [{:keys [body] :as req}]
                          (prn req)
                          (pp/pprint (-> (select-keys req (cond-> [:request-method :headers]
                                                            body (conj :body)))
                                         (update :body slurp)))
                          
                          {:status 200})]
               ["/update" (fn [{:keys [body]}]
                            {:body (edn->json (update (json->edn (slurp body)) :a inc))
                             :status 200})]])
   (ring/routes
    (ring/create-file-handler {:root (or root-dir ".") ;; resource directory to be mounted
                               :path (or base-path "/")}) ;; base path to access by URL
    (ring/create-default-handler))
   {:executor reitit.interceptor.sieppari/executor
    :interceptors [(muuntaja.interceptor/params-interceptor)]}))

(defstate server
  :start  (srv/run-server (app (mount/args)) (merge {:port 9180} (select-keys (mount/args) [:port]))) 
  :stop (server))

(def specs [[nil "--help" "This help"]
            ["-p" "--port <port>" "listen port"
             :default 9180
             :parse-fn parse-long]
            ["-x" "--base-path <path>"
             :default "/"]
            ["-h" "--host <host>" "listen host"
             :default "0.0.0.0"]])

(defn usage [summary]
  (->> ["Usage: slv http <listen> [options]" 
        ""
        "sub commands:"
        ""
        " - listen    listen on specified port"
        ""
        "options:"
        " listen [root directory] : default '.'"
        ""
        "global options:"
        ""
        summary
        ""]
      (str/join \newline)))

(defn main [& args]
  (let [{:keys [options arguments summary]} (parse-opts args specs)]
    (if (:help options)
      (println (usage summary))
      (condp = (first arguments)
        "listen" (-> (mount/only #{#'server})
                     (mount/with-args (assoc options :root-dir (or (second arguments) ".")))
                     (mount/start))
        nil (println (usage summary))
        (println "Not support" (first arguments))))))




