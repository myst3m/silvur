# Silvur

Silvur is the set of functions to handle time, mail and so on.

[![Clojars Project](https://img.shields.io/clojars/v/silvur.svg)](https://clojars.org/silvur)

## Usage

### datetime
```clojure
(require '[slvur.datetime :refer :all])

;; Returns ZonedDatetime class.
;; #object[java.time.ZonedDateTime 0x37910062 "2016-12-24T01:55:44.316+09:00[Asia/Tokyo]"]
(datetime)

;; Milli econds from epoch.
;; Basically this library uses Milli seconds as a base precision.
;; If you want to change seconds, it can be set by using set-default-precision! or binding *precision* to 1000.
(datetime 1482569898624)

;; Force to parse as second
(datetime 1482570090 :second) 

;; String.
(datetime "2016-12-24 01:57:49+09:00")

;; Long.
(datetime 2016 12 24)
(datetime 2016 12 24 1 3)
(datetime 2016 12 24 1 3 20)

;; Date.
;; (Date.) #inst "2016-12-23T17:01:13.860-00:00" 
;; Returns with Time Zone #object[java.time.ZonedDateTime 0x5e02c7fb "2016-12-24T02:01:17+09:00[Asia/Tokyo]"]
(datetime (Date.))  

;; Maps
;; If maps has the key :datetime, datetime uses the value of the key.
(datetime {:datetime 1482512259 :name "myst3m" :id 1001})

;; datetime* returns the seconds from epoch.
;; And also handle the same argument as datetime.
(datetime*)


;; To java.util.GregorianCalendar class
(calender<- (datetime))

;; To vector
(vec<- (datetime))

;; To Instant
(inst<- (datetime))

;; To Date
(date<- (datetime))

;; Return first date of week. Sunday is the first day.
(first-date-of-week (datetime))

;; Return 00:00 of today
(today)

;; Return an instance on 12:34 of today
(today 12 34)

;; Return the same time of yesterday
(apply yesterday (drop 3 (vec<- (datetime))))


;; Return lazy sequence of time seq which has duration between each item.
;; The following example returns time sequence with 2 minute duration.
(take 10 (time-seq (datetime) (minute 2))

```



## License

Copyright © 2016 Tsutomu Miyashita.

Distributed under the Eclipse Public License either version 1.0 or any later version.
